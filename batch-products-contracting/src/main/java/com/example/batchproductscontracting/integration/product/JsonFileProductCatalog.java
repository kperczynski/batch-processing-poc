package com.example.batchproductscontracting.integration.product;

import com.example.batchproductscontracting.integration.serialization.json.ObjectMappers;
import com.example.batchproductscontracting.product.catalog.ProductCatalog;
import com.example.batchproductscontracting.product.catalog.ProductConfiguration;
import com.example.batchproductscontracting.product.catalog.ProductId;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.SneakyThrows;
import lombok.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class JsonFileProductCatalog implements ProductCatalog {

    public static final TypeReference<List<JsonProductConfiguration>> PRODUCT_CONFIGURATIONS_TYPEREF = new TypeReference<>() {
    };

    @Override
    @SneakyThrows
    public List<ProductConfiguration> productsConfiguration() {
        final byte[] bytes = readConfigurationFile();
        final List<JsonProductConfiguration> json = ObjectMappers.INSTANCE.readValue(bytes, PRODUCT_CONFIGURATIONS_TYPEREF);

        return toProductConfigurations(json);
    }

    private static List<ProductConfiguration> toProductConfigurations(List<JsonProductConfiguration> json) {
        return json.stream()
                .map(JsonFileProductCatalog::toProductConfiguration)
                .collect(Collectors.toList());
    }

    private static ProductConfiguration toProductConfiguration(JsonProductConfiguration jsonProductConfiguration) {
        return ProductConfiguration.builder()
                .productId(new ProductId(jsonProductConfiguration.getProductId()))
                .attributes(
                        jsonProductConfiguration.getAttributes().stream()
                                .map(JsonFileProductCatalog::toAttibuteConfiguration)
                                .collect(Collectors.toList())
                )
                .build();
    }

    private static ProductConfiguration.AttributeConfiguration toAttibuteConfiguration(JsonAttributeConfiguration jsonAttributeConfiguration) {
        return ProductConfiguration.AttributeConfiguration.builder()
                .name(jsonAttributeConfiguration.getName())
                .type(jsonAttributeConfiguration.getType())
                .required(jsonAttributeConfiguration.isRequired())
                .build();
    }

    @SneakyThrows
    private static byte[] readConfigurationFile() {
        try (InputStream in = new ClassPathResource("/product-catalog.json").getInputStream()) {
            return in.readAllBytes();
        }
    }

    @Value
    public static class JsonProductConfiguration {
        String productId;
        List<JsonAttributeConfiguration> attributes;

        @JsonCreator
        public JsonProductConfiguration(@JsonProperty("productId") String productId,
                                        @JsonProperty("attributes") List<JsonAttributeConfiguration> attributes) {
            this.productId = productId;
            this.attributes = attributes;
        }
    }

    @Value
    public static class JsonAttributeConfiguration {
        String name;
        String type;
        boolean required;

        public JsonAttributeConfiguration(@JsonProperty("name") String name,
                                          @JsonProperty("type") String type,
                                          @JsonProperty("required") boolean required) {
            this.name = name;
            this.type = type;
            this.required = required;
        }
    }
}
