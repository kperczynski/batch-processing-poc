package com.example.batchproductscontracting.integration.persistence.product;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.util.Objects;

@Entity
@Table(name = "product_activation")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductActivationEntity {

    @Id
    private String id;

    @Column(nullable = false)
    private String productId;
    @Column(nullable = false)
    private String clientId;
    @Column(nullable = false)
    private String attributes;
    @Column(nullable = false)
    private Instant createdAt;

    private String correlationId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductActivationEntity)) return false;
        ProductActivationEntity that = (ProductActivationEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
