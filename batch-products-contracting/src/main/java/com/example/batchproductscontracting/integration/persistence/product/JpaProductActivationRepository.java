package com.example.batchproductscontracting.integration.persistence.product;

import com.example.batchprocessing.exceptions.ResourceDoesNotExistException;
import com.example.batchproductscontracting.integration.serialization.json.ObjectMappers;
import com.example.batchproductscontracting.product.activation.ProductActivation;
import com.example.batchproductscontracting.product.activation.ProductActivationRepository;
import com.example.batchproductscontracting.product.catalog.ProductId;
import com.example.batchproductscontracting.product.activation.ProductOffering;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class JpaProductActivationRepository implements ProductActivationRepository {

    public static final TypeReference<Map<String, String>> PRODUCT_OFFERING_ATTRIBUTES_TYPEREF = new TypeReference<>() {
    };
    private final ProductActivationRepositoryJpa productActivationRepositoryJpa;

    @Override
    public ProductActivation save(ProductActivation customer) {
        final ProductActivationEntity productActivationEntity = toProductActivationEntity(customer);
        productActivationRepositoryJpa.save(productActivationEntity);
        return customer;
    }

    @Override
    public List<ProductActivation> findByClientId(String clientId) {
        return productActivationRepositoryJpa.findByClientId(clientId).stream()
                .map(JpaProductActivationRepository::toProductActivation)
                .collect(Collectors.toList());
    }

    @Override
    public ProductActivation find(String productActivationId) {
        final ProductActivationEntity activationEntity = productActivationRepositoryJpa.findById(productActivationId)
                .orElseThrow(() -> new ResourceDoesNotExistException("product-activation", productActivationId));
        return toProductActivation(activationEntity);
    }

    @SneakyThrows
    private static ProductActivation toProductActivation(ProductActivationEntity activationEntity) {
        return ProductActivation.builder()
                .id(activationEntity.getId())
                .clientId(activationEntity.getClientId())
                .productOffering(
                        ProductOffering.builder()
                                .productId(new ProductId(activationEntity.getProductId()))
                                .attributes(
                                        ObjectMappers.INSTANCE.readValue(activationEntity.getAttributes(), PRODUCT_OFFERING_ATTRIBUTES_TYPEREF)
                                )
                                .build()
                )
                .correlationId(activationEntity.getCorrelationId())
                .createdAt(activationEntity.getCreatedAt())
                .build();
    }

    @SneakyThrows
    private static ProductActivationEntity toProductActivationEntity(ProductActivation productActivation) {
        return ProductActivationEntity.builder()
                .id(productActivation.getId())
                .clientId(productActivation.getClientId())
                .productId(productActivation.getProductOffering().getProductId().getValue())
                .attributes(ObjectMappers.INSTANCE.writeValueAsString(productActivation.getProductOffering().getAttributes()))
                .correlationId(productActivation.getCorrelationId())
                .createdAt(productActivation.getCreatedAt())
                .build();
    }

}
