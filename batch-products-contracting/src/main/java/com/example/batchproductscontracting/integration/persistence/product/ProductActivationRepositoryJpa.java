package com.example.batchproductscontracting.integration.persistence.product;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductActivationRepositoryJpa extends JpaRepository<ProductActivationEntity, String> {
    List<ProductActivationEntity> findByClientId(String clientId);
}
