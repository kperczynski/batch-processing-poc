package com.example.batchproductscontracting.api;

import com.example.batchproductscontracting.product.activation.ProductActivation;
import com.example.batchproductscontracting.product.activation.ProductActivationService;
import com.example.batchproductscontracting.product.activation.ProductOffering;
import com.example.batchproductscontracting.product.catalog.ProductId;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequiredArgsConstructor
public class ProductActivationController {

    private final ProductActivationService productActivationService;

    @PutMapping("/product-activations")
    public ResponseEntity<ProductActivation> activateProduct(@RequestBody ActivateProductRequest activateProductRequest) {
        final ProductActivation productActivation = productActivationService.activateProduct(
                activateProductRequest.getClientId(),
                ProductOffering.builder()
                        .productId(new ProductId(activateProductRequest.getProductId()))
                        .attributes(activateProductRequest.getAttributes())
                        .build()
        );
        return ResponseEntity.ok(productActivation);
    }

    @GetMapping("/product-activations/{activationId}")
    public ResponseEntity<ProductActivation> activateProduct(@PathVariable("activationId") String activationId) {
        final ProductActivation productActivation = productActivationService.readProductActivation(activationId);
        return ResponseEntity.ok(productActivation);
    }

    @Value
    public static class ActivateProductRequest {

        String clientId;
        String productId;
        Map<String, String> attributes;

        public ActivateProductRequest(@JsonProperty("clientId") String clientId,
                                      @JsonProperty("productId") String productId,
                                      @JsonProperty("attributes") Map<String, String> attributes) {
            this.clientId = clientId;
            this.productId = productId;
            this.attributes = attributes;
        }
    }
}
