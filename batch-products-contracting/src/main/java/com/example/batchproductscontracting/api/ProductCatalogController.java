package com.example.batchproductscontracting.api;

import com.example.batchproductscontracting.product.catalog.ProductCatalog;
import com.example.batchproductscontracting.product.catalog.ProductConfiguration;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ProductCatalogController {

    private final ProductCatalog productCatalog;

    @GetMapping("/product-catalog")
    public ResponseEntity<List<ProductConfiguration>> readProductCatalog() {
        final List<ProductConfiguration> configurations = productCatalog.productsConfiguration();
        return ResponseEntity.ok(configurations);
    }

}
