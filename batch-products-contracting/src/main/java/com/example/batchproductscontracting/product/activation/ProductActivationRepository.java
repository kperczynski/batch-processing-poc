package com.example.batchproductscontracting.product.activation;

import java.util.List;

public interface ProductActivationRepository {

    ProductActivation save(ProductActivation productOffering);

    List<ProductActivation> findByClientId(String clientId);

    ProductActivation find(String productActivationId);

}
