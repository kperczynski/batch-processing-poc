package com.example.batchproductscontracting.product.activation;

import com.example.batchprocessing.exceptions.UserErrorException;
import com.example.batchproductscontracting.product.catalog.ProductCatalog;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionOperations;

import java.time.Instant;
import java.util.List;

import static com.example.batchprocessing.correlation.CorrelationIdContext.getCurrentCorrelationId;
import static com.example.batchprocessing.libs.UUIDs.nextUuid;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductActivationService {

    private final ProductCatalog productCatalog;
    private final ProductActivationRepository productActivationRepository;
    private final TransactionOperations transactionOperations;

    public ProductActivation activateProduct(String clientId, ProductOffering productOffering) {
        log.info("Activating product: {} for client: {} with attributes: {}", productOffering.getProductId(), clientId, productOffering.getAttributes());
        checkProductIsAvailableInProductCatalog(productOffering, productCatalog);

        return transactionOperations.execute(status -> {
            final List<ProductActivation> activations = productActivationRepository.findByClientId(clientId);
            final ProductActivation existingProductActivation = findExistingProductActivation(productOffering, activations);

            if (existingActivationMatchesTheCorrelation(existingProductActivation)) {
                return existingProductActivation;
            }

            return productActivationRepository.save(
                    ProductActivation.builder()
                            .id(nextUuid())
                            .clientId(clientId)
                            .correlationId(getCurrentCorrelationId())
                            .productOffering(productOffering)
                            .createdAt(Instant.now())
                            .build()
            );
        });
    }

    private static boolean existingActivationMatchesTheCorrelation(ProductActivation existingProductActivation) {
        if (existingProductActivation == null) {
            return false;
        }
        if (existingProductActivation.getCorrelationId() == null) {
            return false;
        }
        return existingProductActivation.getCorrelationId().equals(getCurrentCorrelationId());
    }

    @Nullable
    private static ProductActivation findExistingProductActivation(ProductOffering productOffering, List<ProductActivation> activations) {
        return activations.stream()
                .filter(activation -> activation.getProductOffering().getProductId().equals(productOffering.getProductId()))
                .findFirst()
                .orElse(null);
    }

    public ProductActivation readProductActivation(String productActivationId) {
        log.info("Reading product activation with id: {}", productActivationId);
        return productActivationRepository.find(productActivationId);
    }

    private static void checkProductIsAvailableInProductCatalog(ProductOffering productOffering, ProductCatalog productCatalog) {
        if (!productCatalog.listAvailableProducts().contains(productOffering.getProductId())) {
            log.warn("Requested an illegal product: {} that is not available in product catalog anymore", productOffering.getProductId());
            throw new UserErrorException("illegal-product-id");
        }
    }

}
