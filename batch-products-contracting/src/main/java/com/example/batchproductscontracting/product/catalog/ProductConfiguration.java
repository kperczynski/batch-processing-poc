package com.example.batchproductscontracting.product.catalog;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class ProductConfiguration {

    ProductId productId;
    List<AttributeConfiguration> attributes;

    @Value
    @Builder
    public static class AttributeConfiguration {
        String name;
        String type;
        boolean required;
    }

}
