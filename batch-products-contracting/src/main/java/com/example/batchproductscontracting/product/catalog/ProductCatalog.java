package com.example.batchproductscontracting.product.catalog;

import java.util.List;
import java.util.stream.Collectors;

public interface ProductCatalog {

    default List<ProductId> listAvailableProducts() {
        return productsConfiguration().stream()
                .map(ProductConfiguration::getProductId)
                .collect(Collectors.toList());
    }
    List<ProductConfiguration> productsConfiguration();

}
