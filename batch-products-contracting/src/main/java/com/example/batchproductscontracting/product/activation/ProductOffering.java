package com.example.batchproductscontracting.product.activation;

import com.example.batchproductscontracting.product.catalog.ProductId;
import lombok.Builder;
import lombok.Value;

import java.util.Map;

@Value
@Builder
public class ProductOffering {

    ProductId productId;
    Map<String, String> attributes;

}
