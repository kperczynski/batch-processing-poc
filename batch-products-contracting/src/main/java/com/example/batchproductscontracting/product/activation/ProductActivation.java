package com.example.batchproductscontracting.product.activation;

import lombok.Builder;
import lombok.Value;
import org.springframework.lang.Nullable;

import java.time.Instant;

@Value
@Builder
public class ProductActivation {

    String id;
    String clientId;
    ProductOffering productOffering;
    Instant createdAt;

    @Nullable
    String correlationId;

}
