package com.example.batchproductscontracting.product.catalog;

import lombok.Value;

@Value
public class ProductId {

    String value;

}
