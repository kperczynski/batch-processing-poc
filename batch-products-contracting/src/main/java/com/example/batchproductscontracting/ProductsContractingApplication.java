package com.example.batchproductscontracting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductsContractingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductsContractingApplication.class, args);
    }

}
