package com.example.batchproductscontracting;

import com.example.batchproductscontracting.integration.serialization.json.ObjectMappers;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProductsContractingConfiguration {

    @Bean
    public ObjectMapper objectMapper() {
        return ObjectMappers.INSTANCE;
    }

}
