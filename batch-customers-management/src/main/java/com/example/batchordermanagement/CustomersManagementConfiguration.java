package com.example.batchordermanagement;

import com.example.batchordermanagement.integrations.kafka.CustomerManagementEventsStreamConfiguration;
import com.example.batchordermanagement.integrations.serialization.json.ObjectMappers;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
        CustomerManagementEventsStreamConfiguration.class
})
public class CustomersManagementConfiguration {

    @Bean
    public ObjectMapper objectMapper() {
        return ObjectMappers.INSTANCE;
    }

}
