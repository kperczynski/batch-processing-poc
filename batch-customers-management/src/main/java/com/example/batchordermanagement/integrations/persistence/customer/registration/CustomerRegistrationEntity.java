package com.example.batchordermanagement.integrations.persistence.customer.registration;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.util.Objects;

@Entity
@Table(name = "customer_registration")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerRegistrationEntity {

    @Id
    private String id;

    @Column(nullable = false)
    private String state;

    @Column(nullable = false)
    private String customerForm;

    private String customer;

    @Column(nullable = false)
    private Instant createdAt;

    private String correlationId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomerRegistrationEntity)) return false;
        CustomerRegistrationEntity that = (CustomerRegistrationEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
