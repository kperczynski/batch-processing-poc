package com.example.batchordermanagement.integrations.persistence.customer.registration;

import com.example.batchordermanagement.customer.Customer;
import com.example.batchordermanagement.customer.CustomerForm;
import com.example.batchordermanagement.customer.registration.CustomerRegistration;
import com.example.batchordermanagement.customer.registration.CustomerRegistrationRepository;
import com.example.batchordermanagement.integrations.serialization.json.ObjectMappers;
import com.example.batchprocessing.exceptions.ResourceDoesNotExistException;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Repository;

@Repository
@AllArgsConstructor
public class JpaCustomerRegistrationRepository implements CustomerRegistrationRepository {

    private final CustomerRegistrationRepositoryJpa customerRegistrationRepositoryJpa;

    @Override
    public CustomerRegistration find(String id) {
        final CustomerRegistrationEntity customerRegistrationEntity = customerRegistrationRepositoryJpa.findById(id)
                .orElseThrow(() -> new ResourceDoesNotExistException("customer-registration", id));
        return toCustomerRegistration(customerRegistrationEntity);
    }

    @Override
    public CustomerRegistration save(CustomerRegistration customerRegistration) {
        final CustomerRegistrationEntity customerRegistrationEntity = toCustomerRegistrationEntity(customerRegistration);
        customerRegistrationRepositoryJpa.save(customerRegistrationEntity);
        return customerRegistration;
    }

    @SneakyThrows
    private static CustomerRegistration toCustomerRegistration(CustomerRegistrationEntity customerRegistrationEntity) {
        return CustomerRegistration.builder()
                .id(customerRegistrationEntity.getId())
                .state(customerRegistrationEntity.getState())
                .correlationId(customerRegistrationEntity.getCorrelationId())
                .customerForm(
                        ObjectMappers.INSTANCE.readValue(customerRegistrationEntity.getCustomerForm(), CustomerForm.class)
                )
                .customer(
                        customerRegistrationEntity.getCustomer() != null
                                ? ObjectMappers.INSTANCE.readValue(customerRegistrationEntity.getCustomer(), Customer.class)
                                : null
                )
                .createdAt(customerRegistrationEntity.getCreatedAt())
                .build();
    }

    @SneakyThrows
    private static CustomerRegistrationEntity toCustomerRegistrationEntity(CustomerRegistration customerRegistration) {
        return CustomerRegistrationEntity.builder()
                .id(customerRegistration.getId())
                .state(customerRegistration.getState())
                .correlationId(customerRegistration.getCorrelationId())
                .customerForm(
                        ObjectMappers.INSTANCE.writeValueAsString(customerRegistration.getCustomerForm())
                )
                .customer(
                        customerRegistration.getCustomer() != null
                                ? ObjectMappers.INSTANCE.writeValueAsString(customerRegistration.getCustomer())
                                : null
                )
                .createdAt(customerRegistration.getCreatedAt())
                .build();
    }
}
