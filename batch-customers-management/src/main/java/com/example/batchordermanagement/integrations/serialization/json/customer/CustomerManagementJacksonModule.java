package com.example.batchordermanagement.integrations.serialization.json.customer;

import com.example.batchordermanagement.customer.Customer;
import com.example.batchordermanagement.customer.CustomerForm;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class CustomerManagementJacksonModule {

    public static SimpleModule customerManagementJacksonModule() {
        final SimpleModule module = new SimpleModule("CustomerManagementJacksonModule");
        module.setMixInAnnotation(CustomerForm.class, CustomerManagementMixins.CustomerFormMixin.class);
        module.setMixInAnnotation(Customer.class, CustomerManagementMixins.CustomerMixin.class);
        return module;
    }

}
