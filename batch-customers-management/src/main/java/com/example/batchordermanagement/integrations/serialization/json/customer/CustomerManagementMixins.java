package com.example.batchordermanagement.integrations.serialization.json.customer;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerManagementMixins {

    public static class CustomerFormMixin {
        @JsonCreator
        public CustomerFormMixin(
                @JsonProperty("firstName") String firstName,
                @JsonProperty("lastName") String lastName,
                @JsonProperty("oib") String oib) {
        }
    }

    public static class CustomerMixin {
        @JsonCreator
        public CustomerMixin(
                @JsonProperty("id") String id,
                @JsonProperty("firstName") String firstName,
                @JsonProperty("lastName") String lastName,
                @JsonProperty("oib") String oib) {
        }
    }

}
