package com.example.batchordermanagement.integrations.kafka;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.Message;

@MessagingGateway
public interface CustomerManagementEventsGateway {

    @Gateway(requestChannel = CustomerManagementEventsStream.OUT)
    void send(Message<?> message);

}
