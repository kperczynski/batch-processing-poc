package com.example.batchordermanagement.integrations.kafka;

import com.example.batchordermanagement.customer.registration.CustomerRegistration;
import com.example.batchordermanagement.customer.registration.CustomerRegistrationEvents;
import com.example.batchordermanagement.integrations.serialization.json.ObjectMappers;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
@RequiredArgsConstructor
public class KafkaCustomerRegistrationEvents implements CustomerRegistrationEvents {

    private final CustomerManagementEventsGateway customerManagementEventsGateway;

    @Override
    @SneakyThrows
    public void sendCustomerRegistrationCompleted(CustomerRegistration customerRegistration) {
        final String payload = ObjectMappers.INSTANCE.writeValueAsString(Map.of(
                "type", "CUSTOMER_REGISTRATION_COMPLETED",
                "registrationId", customerRegistration.getId(),
                "customerId", customerRegistration.getCustomer().getId()
        ));
        customerManagementEventsGateway.send(
                new GenericMessage<>(payload, toKafkaHeaders(customerRegistration))
        );
    }

    @Override
    @SneakyThrows
    public void sendCustomerRegistrationError(CustomerRegistration customerRegistration) {
        final String payload = ObjectMappers.INSTANCE.writeValueAsString(Map.of(
                "type", "CUSTOMER_REGISTRATION_ERROR",
                "registrationId", customerRegistration.getId()
        ));
        customerManagementEventsGateway.send(
                new GenericMessage<>(payload, toKafkaHeaders(customerRegistration))
        );
    }

    private static Map<String, Object> toKafkaHeaders(CustomerRegistration customerRegistration) {
        if (customerRegistration.getCorrelationId() == null) {
            return Map.of();
        }
        return Map.of(KafkaHeaders.CORRELATION_ID, customerRegistration.getCorrelationId());
    }

}
