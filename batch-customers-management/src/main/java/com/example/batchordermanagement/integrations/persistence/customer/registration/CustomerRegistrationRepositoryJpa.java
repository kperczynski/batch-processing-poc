package com.example.batchordermanagement.integrations.persistence.customer.registration;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRegistrationRepositoryJpa extends JpaRepository<CustomerRegistrationEntity, String> {

}
