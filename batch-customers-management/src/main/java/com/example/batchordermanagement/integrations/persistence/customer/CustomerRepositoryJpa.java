package com.example.batchordermanagement.integrations.persistence.customer;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepositoryJpa extends JpaRepository<CustomerEntity, String> {
}
