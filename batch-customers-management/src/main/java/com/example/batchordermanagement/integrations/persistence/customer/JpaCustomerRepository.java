package com.example.batchordermanagement.integrations.persistence.customer;

import com.example.batchordermanagement.customer.Customer;
import com.example.batchordermanagement.customer.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class JpaCustomerRepository implements CustomerRepository {

    private final CustomerRepositoryJpa customerRepositoryJpa;

    @Override
    public Customer save(Customer customer) {
        final CustomerEntity customerEntity = toCustomerEntity(customer);
        customerRepositoryJpa.save(customerEntity);
        return customer;
    }

    private static CustomerEntity toCustomerEntity(Customer customer) {
        return CustomerEntity.builder()
                .id(customer.getId())
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .oib(customer.getOib())
                .build();
    }

}
