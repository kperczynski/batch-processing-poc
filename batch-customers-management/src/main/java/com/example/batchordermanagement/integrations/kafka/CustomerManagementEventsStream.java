package com.example.batchordermanagement.integrations.kafka;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface CustomerManagementEventsStream {

    String OUT = "out-customer-management-events";

    @Output(OUT)
    MessageChannel outputChannel();

}
