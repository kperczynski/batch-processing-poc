package com.example.batchordermanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomersManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomersManagementApplication.class, args);
    }

}
