package com.example.batchordermanagement.libs;

import brave.Span;
import brave.Tracer;
import brave.propagation.TraceContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

@Component
@RequiredArgsConstructor
public class RealFutureOperations implements FutureOperations {

    private final Tracer tracer;

    @Override
    public CompletableFuture<Void> schedule(Runnable runnable, long millis) {
        final TraceContext context = tracer.nextSpan().context();
        return CompletableFuture.runAsync(() -> {
            final Span span = tracer.newChild(context);
            try (Tracer.SpanInScope $ = tracer.withSpanInScope(span)) {
                Thread.sleep(millis);
                runnable.run();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(e);
            }
            finally {
                span.finish();
            }
        });
    }
}
