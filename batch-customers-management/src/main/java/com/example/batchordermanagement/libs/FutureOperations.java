package com.example.batchordermanagement.libs;

import java.util.concurrent.CompletableFuture;

public interface FutureOperations {

    CompletableFuture<Void> schedule(Runnable runnable, long millis);

}
