package com.example.batchordermanagement.customer;

public interface CustomerRepository {

    Customer save(Customer customer);

}
