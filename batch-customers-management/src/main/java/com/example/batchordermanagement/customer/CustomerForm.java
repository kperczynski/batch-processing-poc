package com.example.batchordermanagement.customer;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class CustomerForm {

    String firstName;
    String lastName;
    String oib;

}
