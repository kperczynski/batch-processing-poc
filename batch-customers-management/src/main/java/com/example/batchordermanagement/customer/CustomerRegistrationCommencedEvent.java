package com.example.batchordermanagement.customer;

import lombok.Value;

@Value
public class CustomerRegistrationCommencedEvent {

    String registrationOd;

}
