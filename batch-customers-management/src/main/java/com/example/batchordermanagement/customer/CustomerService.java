package com.example.batchordermanagement.customer;

import com.example.batchordermanagement.customer.registration.CustomerRegistration;
import com.example.batchordermanagement.customer.registration.CustomerRegistrationEvents;
import com.example.batchordermanagement.customer.registration.CustomerRegistrationRepository;
import com.example.batchordermanagement.libs.FutureOperations;
import com.example.batchprocessing.exceptions.UserErrorException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionOperations;

import java.time.Instant;
import java.util.Set;

import static com.example.batchprocessing.correlation.CorrelationIdContext.getCurrentCorrelationId;
import static com.example.batchprocessing.libs.UUIDs.nextUuid;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerService {

    public static final Set<String> PUBLICLY_EXPOSED_PERSONS = Set.of("Jeff Bezos", "Elon Musk", "Donald Trump", "Angela Merkel", "Vladimir Putin");
    public static final Set<String> BANNED_PERSONS = Set.of("Linus Torvalds");

    private final CustomerRepository customerRepository;
    private final CustomerRegistrationRepository customerRegistrationRepository;
    private final TransactionOperations transactionOperations;
    private final FutureOperations futureOperations;
    private final CustomerRegistrationEvents customerRegistrationEvents;

    public CustomerRegistration commenceCustomerRegistration(CustomerForm customerForm) {
        log.info("Commencing customer registration for incoming customer: {}", customerForm);
        checkIncomingPersonIsNotBanned(customerForm);

        final CustomerRegistration registration = transactionOperations.execute(status ->
                customerRegistrationRepository.save(
                        CustomerRegistration.builder()
                                .id(nextUuid())
                                .state("IDLE")
                                .customerForm(customerForm)
                                .customer(null)
                                .createdAt(Instant.now())
                                .correlationId(getCurrentCorrelationId())
                                .build()
                )
        );
        futureOperations.schedule(() -> registerCustomer(registration.getId()), 1000L);
        futureOperations.schedule(
                () -> completeCustomerRegistration(registration.getId()),
                5000L
        );
        return registration;
    }

    private CustomerRegistration completeCustomerRegistration(String registrationId) {
        log.info("Completing customer registration: {}", registrationId);
        final CustomerRegistration registration = customerRegistrationRepository.find(registrationId);

        if (isPubliclyExposedPerson(registration.getCustomerForm())) {
            log.info("Failed to register customer with registration: {}", registration.getId());
            final CustomerRegistration customerRegistration = transactionOperations.execute(status ->
                    customerRegistrationRepository.save(
                            registration.toBuilder()
                                    .state("ERROR")
                                    .build()
                    )
            );
            customerRegistrationEvents.sendCustomerRegistrationError(customerRegistration);
            return customerRegistration;
        }

        final CustomerRegistration customerRegistration = transactionOperations.execute(status -> {
            final Customer customer = toCustomer(registration);
            log.info("Successfully registered customer: {}", customer.getId());
            customerRepository.save(customer);
            return customerRegistrationRepository.save(
                    registration.toBuilder()
                            .state("COMPLETED")
                            .customer(customer)
                            .build()
            );
        });
        customerRegistrationEvents.sendCustomerRegistrationCompleted(customerRegistration);
        return customerRegistration;
    }

    private static void checkIncomingPersonIsNotBanned(CustomerForm customerForm) {
        final String fullName = String.format("%s %s", customerForm.getFirstName(), customerForm.getLastName());
        if (BANNED_PERSONS.stream()
                .anyMatch(personsFullName -> personsFullName.equalsIgnoreCase(fullName))) {
            throw new UserErrorException("incoming-person-is-banned");
        }
    }

    private static boolean isPubliclyExposedPerson(CustomerForm customerForm) {
        final String fullName = String.format("%s %s", customerForm.getFirstName(), customerForm.getLastName());
        return PUBLICLY_EXPOSED_PERSONS.stream()
                .anyMatch(personsFullName -> personsFullName.equalsIgnoreCase(fullName));
    }

    private CustomerRegistration registerCustomer(String registrationId) {
        log.info("Registering customer for customer registration: {}", registrationId);
        final CustomerRegistration registration = customerRegistrationRepository.find(registrationId);
        return transactionOperations.execute(status ->
                customerRegistrationRepository.save(
                        registration.toBuilder()
                                .state("IN_PROGRESS")
                                .build()
                )
        );
    }

    private static Customer toCustomer(CustomerRegistration registration) {
        return Customer.builder()
                .id(nextUuid())
                .firstName(registration.getCustomerForm().getFirstName())
                .lastName(registration.getCustomerForm().getLastName())
                .oib(registration.getCustomerForm().getOib())
                .build();
    }

    public CustomerRegistration readCustomerRegistration(String registrationId) {
        log.info("Reading customer registration: {}", registrationId);
        return customerRegistrationRepository.find(registrationId);
    }

}
