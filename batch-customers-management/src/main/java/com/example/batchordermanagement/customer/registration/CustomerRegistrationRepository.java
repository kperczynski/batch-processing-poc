package com.example.batchordermanagement.customer.registration;

public interface CustomerRegistrationRepository {

    CustomerRegistration find(String id);
    CustomerRegistration save(CustomerRegistration customerRegistration);

}
