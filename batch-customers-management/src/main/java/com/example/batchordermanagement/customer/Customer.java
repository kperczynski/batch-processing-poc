package com.example.batchordermanagement.customer;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Customer {

    String id;
    String firstName;
    String lastName;
    String oib;

}
