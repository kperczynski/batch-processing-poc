package com.example.batchordermanagement.customer.registration;

import com.example.batchordermanagement.customer.Customer;
import com.example.batchordermanagement.customer.CustomerForm;
import lombok.Builder;
import lombok.Value;
import org.springframework.lang.Nullable;

import java.time.Instant;

@Value
@Builder(toBuilder = true)
public class CustomerRegistration {

    String id;
    String state;
    CustomerForm customerForm;

    @Nullable
    Customer customer;
    Instant createdAt;
    @Nullable
    String correlationId;

}
