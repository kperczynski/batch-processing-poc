package com.example.batchordermanagement.customer.registration;

public interface CustomerRegistrationEvents {

    void sendCustomerRegistrationCompleted(CustomerRegistration customerRegistration);

    void sendCustomerRegistrationError(CustomerRegistration customerRegistration);

}
