package com.example.batchordermanagement.api;

import com.example.batchordermanagement.customer.CustomerForm;
import com.example.batchordermanagement.customer.CustomerService;
import com.example.batchordermanagement.customer.registration.CustomerRegistration;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping("/customer-registrations")
    public ResponseEntity<CustomerRegistration> commenceCustomerRegistration(@RequestBody CustomerForm customerForm) {
        final CustomerRegistration customerRegistration = customerService.commenceCustomerRegistration(customerForm);
        return ResponseEntity.ok(customerRegistration);
    }

    @GetMapping("/customer-registrations/{registrationId}")
    public ResponseEntity<CustomerRegistration> readCustomerRegistration(@PathVariable("registrationId") String registrationId) {
        final CustomerRegistration customerRegistration = customerService.readCustomerRegistration(registrationId);
        return ResponseEntity.ok(customerRegistration);
    }

}
