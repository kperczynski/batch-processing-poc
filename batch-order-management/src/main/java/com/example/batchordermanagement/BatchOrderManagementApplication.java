package com.example.batchordermanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchOrderManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(BatchOrderManagementApplication.class, args);
    }

}
