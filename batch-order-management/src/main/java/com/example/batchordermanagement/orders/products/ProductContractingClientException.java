package com.example.batchordermanagement.orders.products;

public class ProductContractingClientException extends RuntimeException {
    public ProductContractingClientException(String message) {
        super(message);
    }

    public ProductContractingClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
