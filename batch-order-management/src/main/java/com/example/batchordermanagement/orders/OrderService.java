package com.example.batchordermanagement.orders;

import com.example.batchordermanagement.orders.order.Order;
import com.example.batchordermanagement.orders.order.OrderArtifacts;
import com.example.batchordermanagement.orders.order.OrderForm;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionOperations;

import java.time.Instant;

import static com.example.batchprocessing.libs.UUIDs.nextUuid;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrdersRepository ordersRepository;
    private final TransactionOperations transactionOperations;
    private final OrderProcessingJobLauncher orderProcessingJobLauncher;

    public Order createOrder(OrderForm form) {
        log.info("Creating order with form: {}", form);
        final Order createdOrder = transactionOperations.execute(status ->
                ordersRepository.save(
                        Order.builder()
                                .id(nextUuid())
                                .form(form)
                                .artifacts(OrderArtifacts.of())
                                .state(OrderStates.ISSUED)
                                .createdAt(Instant.now())
                                .lastModified(Instant.now())
                                .build()
                )
        );
        orderProcessingJobLauncher.runOrderProcessing(createdOrder);
        return createdOrder;
    }

    @SneakyThrows
    public void notifyCustomerRegistrationCompleted(String orderId, String customerId) {
        log.info("Confirming customer registration for order: {} and customer: {}", orderId, customerId);
        final Order updatedOrder = transactionOperations.execute(status -> {
            final Order foundOrder = ordersRepository.find(orderId);
            if (!foundOrder.getState().equals(OrderStates.CUSTOMER_REGISTRATION_COMMENCEMENT_COMPLETED)) {
                throw new IllegalStateException("Illegal state of the order");
            }
            return ordersRepository.save(
                    foundOrder.toBuilder()
                            .artifacts(
                                    foundOrder.getArtifacts()
                                            .withArtifact(
                                                    OrderArtifactKeys.REGISTERED_CUSTOMER_ID.name(),
                                                    customerId
                                            )
                            )
                            .state(OrderStates.CUSTOMER_REGISTRATION_COMPLETED)
                            .lastModified(Instant.now())
                            .build()
            );
        });
        orderProcessingJobLauncher.runOrderProcessing(updatedOrder);
    }

    public void notifyCustomerRegistrationFailed(String orderId) {
        log.info("Notifying failure of the customer registration for order: {}", orderId);
        transactionOperations.execute(status -> {
            final Order foundOrder = ordersRepository.find(orderId);
            return ordersRepository.save(
                    foundOrder.toBuilder()
                            .state(OrderStates.CUSTOMER_REGISTRATION_ERROR)
                            .lastModified(Instant.now())
                            .build()
            );
        });
    }

    public Order readOrder(String orderId) {
        log.info("Reading order with id: {}", orderId);
        return ordersRepository.find(orderId);
    }
}
