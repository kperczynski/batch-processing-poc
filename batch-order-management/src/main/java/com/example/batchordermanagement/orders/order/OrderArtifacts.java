package com.example.batchordermanagement.orders.order;

import lombok.Value;

import java.util.HashMap;
import java.util.Map;

public interface OrderArtifacts {

    static OrderArtifacts of() {
        return new DefaultOrderArtifacts(Map.of());
    }

    OrderArtifacts withArtifact(String key, Object value);

    <T> T findArtifact(String key, Class<T> clazz);

    Map<String, Object> toMap();

    @Value
    class DefaultOrderArtifacts implements OrderArtifacts {

        Map<String, Object> artifacts;

        @Override
        public OrderArtifacts withArtifact(String key, Object value) {
            final Map<String, Object> copiedArtifacts = new HashMap<>(artifacts);
            copiedArtifacts.put(key, value);
            return new DefaultOrderArtifacts(copiedArtifacts);
        }

        @Override
        public <T> T findArtifact(String key, Class<T> clazz) {
            final Object item = artifacts.get(key);
            return clazz.cast(item);
        }

        @Override
        public Map<String, Object> toMap() {
            return new HashMap<>(artifacts);
        }

    }

}
