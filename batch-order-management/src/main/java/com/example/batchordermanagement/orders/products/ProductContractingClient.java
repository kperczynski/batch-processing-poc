package com.example.batchordermanagement.orders.products;

import com.example.batchordermanagement.orders.order.ProductOffering;

public interface ProductContractingClient  {

    ProductActivation contractProduct(String clientId, ProductOffering productOffering);

}
