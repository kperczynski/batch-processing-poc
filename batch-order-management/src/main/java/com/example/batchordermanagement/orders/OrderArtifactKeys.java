package com.example.batchordermanagement.orders;

public enum OrderArtifactKeys {

    CUSTOMER_REGISTRATION_PROCESS_ID,
    REGISTERED_CUSTOMER_ID,
    CONTRACTED_PRODUCTS

}
