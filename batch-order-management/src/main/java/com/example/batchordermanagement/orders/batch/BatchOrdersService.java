package com.example.batchordermanagement.orders.batch;

import com.example.batchordermanagement.orders.OrdersOperations;
import com.example.batchordermanagement.orders.order.Order;
import com.example.batchordermanagement.orders.products.ProductContractingOperations;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class BatchOrdersService {

    private final OrdersOperations ordersOperations;
    private final ProductContractingOperations productContractingOperations;

    public Order commenceCustomerRegistration(Order order) {
        log.info("Commencing incoming customer registration for order: {}", order.getId());
        return ordersOperations.commenceIncomingCustomerRegistration(order);
    }

    public Order contractOrderedProducts(Order order) {
        log.info("Contracting ordered products for order: {}", order.getId());
        return productContractingOperations.contractProducts(order);
    }

}
