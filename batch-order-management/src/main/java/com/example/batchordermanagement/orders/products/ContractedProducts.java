package com.example.batchordermanagement.orders.products;

import com.example.batchordermanagement.orders.order.ProductOffering;
import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class ContractedProducts {

    public static final ContractedProducts EMPTY_CONTRACTED_PRODUCTS = new ContractedProducts(List.of());

    List<ContractedProduct> contractedProducts;

    @Value
    @Builder
    public static class ContractedProduct {

        String activationId;
        ProductOffering productOffering;

    }

}
