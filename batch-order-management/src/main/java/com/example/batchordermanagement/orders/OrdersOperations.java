package com.example.batchordermanagement.orders;

import com.example.batchordermanagement.customersmanagement.CustomerRegistrationProcess;
import com.example.batchordermanagement.customersmanagement.CustomersManagementClient;
import com.example.batchordermanagement.orders.order.Order;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrdersOperations {

    private final CustomersManagementClient customersManagementClient;

    public Order commenceIncomingCustomerRegistration(Order order) {
        final CustomerRegistrationProcess customerRegistrationProcess
                = customersManagementClient.commenceCustomerRegistration(order.getForm().getCustomer());

        return order.toBuilder()
                .state(OrderStates.CUSTOMER_REGISTRATION_COMMENCEMENT_COMPLETED)
                .artifacts(
                        order.getArtifacts()
                                .withArtifact(
                                        OrderArtifactKeys.CUSTOMER_REGISTRATION_PROCESS_ID.name(),
                                        customerRegistrationProcess.getId()
                                )
                )
                .build();
    }

}
