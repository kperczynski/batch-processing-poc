package com.example.batchordermanagement.orders;

import com.example.batchordermanagement.orders.order.Order;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;

import java.util.List;

public interface OrdersRepository {

    List<Order> findAllOrdersInState(String state, Pageable pageable);
    Order find(String id);
    @Nullable
    Order tryFindOrderInState(String id, String state);
    @Nullable
    Order trySelectForUpdate(String id);
    Order save(Order order);

}
