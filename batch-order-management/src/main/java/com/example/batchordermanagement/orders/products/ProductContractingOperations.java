package com.example.batchordermanagement.orders.products;

import com.example.batchordermanagement.orders.OrderArtifactKeys;
import com.example.batchordermanagement.orders.OrderStates;
import com.example.batchordermanagement.orders.order.Order;
import com.example.batchordermanagement.orders.order.ProductOffering;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.example.batchordermanagement.orders.products.ContractedProducts.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class ProductContractingOperations {

    private final ProductContractingClient productContractingClient;

    public Order contractProducts(Order order) {
        final ContractedProducts contractedProducts = Objects.requireNonNullElse(
                order.getArtifacts()
                        .findArtifact(OrderArtifactKeys.CONTRACTED_PRODUCTS.name(), ContractedProducts.class),
                EMPTY_CONTRACTED_PRODUCTS
        );
        final String clientId = order.getArtifacts()
                .findArtifact(OrderArtifactKeys.REGISTERED_CUSTOMER_ID.name(), String.class);

        final List<ContractedProduct> updatedContractedProducts = new ArrayList<>(contractedProducts.getContractedProducts());

        for (final ProductOffering orderedProduct : order.getForm().getProducts()) {
            if (isOrderedProductAlreadyContracted(contractedProducts, orderedProduct)) {
                continue;
            }
            log.info("Contracting product: {} for the client: {}", orderedProduct.getProductId(), clientId);
            try {
                final ProductActivation productActivation = productContractingClient.contractProduct(clientId, orderedProduct);
                updatedContractedProducts.add(
                        new ContractedProduct(productActivation.getId(), orderedProduct)
                );
            } catch (ProductContractingClientException e) {
                log.error("Could not contract product: {} due to unexpected exception: {}", orderedProduct.getProductId(), e.getMessage(), e);
            }
        }

        final String nextOrderState = allOrderedProductsAreContracted(updatedContractedProducts, order.getForm().getProducts())
                ? OrderStates.PRODUCTS_CONTRACTING_COMPLETED
                : OrderStates.PRODUCTS_CONTRACTING_PARTIALLY_COMPLETED;

        return order.toBuilder()
                .state(nextOrderState)
                .artifacts(
                        order.getArtifacts()
                                .withArtifact(
                                        OrderArtifactKeys.CONTRACTED_PRODUCTS.name(),
                                        new ContractedProducts(updatedContractedProducts)
                                )
                )
                .lastModified(Instant.now())
                .build();
    }

    private static boolean allOrderedProductsAreContracted(List<ContractedProduct> contractedProducts, List<ProductOffering> orderedProducts) {
        final Set<String> contractedProductIds = contractedProducts.stream()
                .map(contractedProduct -> contractedProduct.getProductOffering().getProductId())
                .collect(Collectors.toSet());
        final Set<String> orderedProductIds = orderedProducts.stream()
                .map(ProductOffering::getProductId)
                .collect(Collectors.toSet());

        return contractedProductIds.containsAll(orderedProductIds);
    }

    private static boolean isOrderedProductAlreadyContracted(ContractedProducts contractedProducts, ProductOffering product) {
        return contractedProducts.getContractedProducts().stream()
                .anyMatch(contractedProduct -> contractedProduct.getProductOffering().getProductId().equals(product.getProductId()));
    }

}
