package com.example.batchordermanagement.orders.order;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class OrderForm {
    IncomingCustomer customer;
    List<ProductOffering> products;
}
