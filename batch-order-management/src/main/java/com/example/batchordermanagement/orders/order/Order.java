package com.example.batchordermanagement.orders.order;

import lombok.Builder;
import lombok.Value;

import java.time.Instant;

@Value
@Builder(toBuilder = true)
public class Order {

    String id;
    String state;
    OrderForm form;
    OrderArtifacts artifacts;
    Instant createdAt;
    Instant lastModified;

}
