package com.example.batchordermanagement.orders.order;

import lombok.Builder;
import lombok.Value;

import java.util.Map;

@Value
@Builder
public class ProductOffering {

    String productId;
    Map<String, String> attributes;

}
