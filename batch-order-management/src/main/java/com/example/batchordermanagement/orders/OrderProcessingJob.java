package com.example.batchordermanagement.orders;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class OrderProcessingJob {

    private final OrderProcessingJobLauncher orderProcessingJobLauncher;

    public void runAllOrdersProcessing() {
        log.trace("Running order processing job");
        orderProcessingJobLauncher.runAllOrdersProcessing();
    }

}
