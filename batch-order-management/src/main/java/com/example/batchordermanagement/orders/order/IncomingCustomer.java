package com.example.batchordermanagement.orders.order;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class IncomingCustomer {

    String firstName;
    String lastName;
    String oib;

}
