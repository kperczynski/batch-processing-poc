package com.example.batchordermanagement.orders;

import com.example.batchordermanagement.orders.order.Order;

public interface OrderProcessingJobLauncher {

    void runOrderProcessing(Order order);

    void runAllOrdersProcessing();

}
