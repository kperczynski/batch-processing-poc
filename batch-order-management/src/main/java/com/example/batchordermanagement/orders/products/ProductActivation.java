package com.example.batchordermanagement.orders.products;

import lombok.Value;

@Value
public class ProductActivation {
    String id;
}
