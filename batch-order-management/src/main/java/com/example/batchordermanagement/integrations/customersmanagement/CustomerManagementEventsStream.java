package com.example.batchordermanagement.integrations.customersmanagement;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface CustomerManagementEventsStream {

    String IN = "in-customer-management-events";

    @Input(IN)
    SubscribableChannel inputChannel();

}
