package com.example.batchordermanagement.integrations.customersmanagement;

import com.example.batchordermanagement.customersmanagement.Customer;
import com.example.batchordermanagement.customersmanagement.CustomerRegistrationProcess;
import com.example.batchordermanagement.customersmanagement.CustomersManagementClient;
import com.example.batchordermanagement.customersmanagement.CustomersManagementClientException;
import com.example.batchordermanagement.integrations.serialization.json.ObjectMappers;
import com.example.batchordermanagement.orders.order.IncomingCustomer;
import com.example.batchprocessing.tracing.HttpClientTracing;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;
import java.util.Objects;

import static com.example.batchprocessing.correlation.CorrelationIdContext.getCurrentCorrelationId;
import static com.example.batchprocessing.logging.HttpClientLogs.logRequest;
import static com.example.batchprocessing.logging.HttpClientLogs.logResponse;

@Component
@Slf4j
public class HttpCustomersManagementClient implements CustomersManagementClient {

    private final HttpClient httpClient;
    private final HttpClientTracing httpClientTracing;
    private final CustomersManagementProperties customersManagementProperties;

    public HttpCustomersManagementClient(@QCustomersManagement HttpClient httpClient,
                                         HttpClientTracing httpClientTracing,
                                         CustomersManagementProperties customersManagementProperties) {
        this.httpClient = httpClient;
        this.httpClientTracing = httpClientTracing;
        this.customersManagementProperties = customersManagementProperties;
    }

    @Override
    public CustomerRegistrationProcess commenceCustomerRegistration(IncomingCustomer incomingCustomer) {
        final String correlationId = Objects.requireNonNull(
                getCurrentCorrelationId(),
                "Missing correlation id for customer registration"
        );
        final URI uri = UriComponentsBuilder.fromUri(customersManagementProperties.getServiceUrl())
                .path("/customer-registrations")
                .build(Map.of());

        try {
            final HttpRequest request = httpClientTracing.tracedHttpRequest()
                    .POST(HttpRequest.BodyPublishers.ofString(
                            toJson(incomingCustomer)
                    ))
                    .uri(uri)
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .header("X-Correlation-Id", correlationId)
                    .build();

            logRequest(log, request);
            final HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            logResponse(log, response);

            if (HttpStatus.valueOf(response.statusCode()).is2xxSuccessful()) {
                final CustomerRegistrationProcessDto responseDto = ObjectMappers.INSTANCE.readValue(
                        response.body(),
                        CustomerRegistrationProcessDto.class
                );
                return toCustomerRegistrationProcess(responseDto);
            }
            throw new CustomersManagementClientException("Unexpected response status: " + response.statusCode());
        } catch (IOException e) {
            throw new CustomersManagementClientException("Unexpected transport layer exception", e);
        } catch (InterruptedException e) {
            throw gracefullyHandleInterruptedException(e);
        }
    }

    private static CustomerRegistrationProcess toCustomerRegistrationProcess(CustomerRegistrationProcessDto registrationProcessDto) {
        return CustomerRegistrationProcess.builder()
                .id(registrationProcessDto.getId())
                .registeredCustomer(
                        toRegisteredCustomer(registrationProcessDto.getCustomer())
                )
                .state(registrationProcessDto.getState())
                .build();
    }

    private static Customer toRegisteredCustomer(@Nullable CustomerRegistrationProcessDto.CustomerDto customer) {
        if (customer == null) {
            return null;
        }
        return Customer.builder()
                .id(customer.getId())
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .oib(customer.getOib())
                .build();
    }

    @SneakyThrows
    private static RuntimeException gracefullyHandleInterruptedException(InterruptedException e) {
        throw e;
    }

    @SneakyThrows
    private static String toJson(Object object) {
        return ObjectMappers.INSTANCE.writeValueAsString(object);
    }
}
