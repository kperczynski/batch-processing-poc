package com.example.batchordermanagement.integrations.customersmanagement;

import com.example.batchordermanagement.integrations.serialization.json.ObjectMappers;
import com.example.batchordermanagement.orders.OrderService;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;

@Slf4j
@Component
@RequiredArgsConstructor
public class CustomerManagementEventsListener {

    public static final TypeReference<Map<String, String>> MAP_TYPEREF = new TypeReference<>() {
    };
    private final OrderService orderService;

    @StreamListener(CustomerManagementEventsStream.IN)
    public void onCustomerManagementEvent(Message<String> message) {
        log.trace("Received customer management event: {}", message);
        try {
            final Map<String, String> payload = toMessage(message);
            final String orderId = Optional.ofNullable(message.getHeaders().get(KafkaHeaders.CORRELATION_ID))
                    .map(Object::toString)
                    .orElseThrow(() -> new IllegalArgumentException("Missing correlation-id header in received Kafka message"));

            switch (payload.get("type")) {
                case "CUSTOMER_REGISTRATION_COMPLETED":
                    orderService.notifyCustomerRegistrationCompleted(orderId, payload.get("customerId"));
                    break;
                case "CUSTOMER_REGISTRATION_ERROR":
                    orderService.notifyCustomerRegistrationFailed(orderId);
                    break;
                default:
                    log.info("Encountered unknown customers management event: {}", payload.get("type"));
            }
        } catch (Exception e) {
            log.error("Unexpected exception in customers management events listener: {}", e.getMessage(), e);
        }
    }

    @SneakyThrows
    private static Map<String, String> toMessage(Message<String> message) {
        return ObjectMappers.INSTANCE.readValue(message.getPayload(), MAP_TYPEREF);
    }

}
