package com.example.batchordermanagement.integrations.batch.orders;

import com.example.batchordermanagement.orders.OrdersRepository;
import com.example.batchordermanagement.orders.order.Order;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.support.TransactionOperations;

import java.util.Optional;

import static com.example.batchprocessing.correlation.CorrelationIdContext.*;

@Slf4j
@RequiredArgsConstructor
public class OrderProcessorTemplate {

    private final TransactionOperations transactionOperations;
    private final OrdersRepository ordersRepository;
    private final String expectedState;
    private final String lockedState;
    private final OrderTracing orderTracing;

    public Order process(String orderId, CheckedFunction<Order, Order> delegate) throws Exception {
        return orderTracing.traceOrder(orderId, () -> {
            setCurrentCorrelationId(orderId);
            final Tuple<Order, Boolean> tuple = tryLock(orderId);
            final Order lockedOrder = tuple._1;
            final Boolean isLocked = tuple._2;
            if (!isLocked) {
                final String reason = Optional.ofNullable(lockedOrder)
                        .map(order -> String.format("expected order state: %s, but was %s", this.expectedState, order.getState()))
                        .orElse("the order could not be found");

                log.debug("Skipping processing of order: {} because {}", orderId, reason);
                return null;
            }
            return delegate.apply(lockedOrder);
        });
    }

    private Tuple<Order, Boolean> tryLock(String itemId) {
        return transactionOperations.execute(status -> {
            final Order order = ordersRepository.trySelectForUpdate(itemId);
            if (order == null) {
                return new Tuple<>(null, false);
            }
            if (!order.getState().equals(expectedState)) {
                return new Tuple<>(order, false);
            }

            final Order lockedOrder = ordersRepository.save(
                    order.toBuilder()
                            .state(lockedState)
                            .build()
            );
            return new Tuple<>(lockedOrder, true);
        });
    }

    public interface CheckedFunction<T, U> {
        U apply(T arg) throws Exception;
    }

    @Value
    private static class Tuple<T, U> {
        T _1;
        U _2;
    }
}
