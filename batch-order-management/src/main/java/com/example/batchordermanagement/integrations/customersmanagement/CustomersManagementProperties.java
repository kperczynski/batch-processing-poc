package com.example.batchordermanagement.integrations.customersmanagement;

import com.sun.istack.NotNull;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import java.net.URI;

@ConfigurationProperties("customers-management")
@Validated
@Data
public class CustomersManagementProperties {

    @NotNull
    private URI serviceUrl;

}
