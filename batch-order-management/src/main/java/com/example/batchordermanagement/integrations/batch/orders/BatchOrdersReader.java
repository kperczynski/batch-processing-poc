package com.example.batchordermanagement.integrations.batch.orders;

import com.example.batchordermanagement.integrations.batch.QueryingItemReader;
import com.example.batchordermanagement.orders.OrdersRepository;
import com.example.batchordermanagement.orders.order.Order;
import lombok.experimental.Delegate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemStreamReader;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class BatchOrdersReader implements ItemStreamReader<String> {

    public static final String ORDER_ID_PARAMETER = "BatchOrdersReader.ORDER_ID_PARAMETER";

    private final OrdersRepository ordersRepository;
    private final String orderState;
    private final Map<String, Object> jobParameters;

    @Delegate
    private final QueryingItemReader<String> queryingItemReader;

    public BatchOrdersReader(OrdersRepository ordersRepository,
                             String orderState,
                             Map<String, Object> jobParameters) {
        this.ordersRepository = ordersRepository;
        this.orderState = orderState;
        this.jobParameters = jobParameters;
        this.queryingItemReader = new QueryingItemReader<>(this::nextBatch);
    }

    private List<String> nextBatch() {
        final Object itemId = jobParameters.get(ORDER_ID_PARAMETER);
        if (itemId instanceof String) {
            final Order foundOrder = ordersRepository.tryFindOrderInState(itemId.toString(), this.orderState);
            final List<String> items = foundOrder != null
                    ? List.of(foundOrder.getId())
                    : List.of();

            logReadItems(items, this.orderState);
            return items;
        }

        final List<String> items = ordersRepository.findAllOrdersInState(orderState, PageRequest.of(0, 5)).stream()
                .map(Order::getId)
                .collect(Collectors.toList());

        logReadItems(items, this.orderState);
        return items;
    }

    private static void logReadItems(List<String> items, String state) {
        log.info("Read batch of next {} new orders in state: {} to process", items.size(), state);
    }

}
