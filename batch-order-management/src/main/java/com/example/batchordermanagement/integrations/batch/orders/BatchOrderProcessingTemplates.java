package com.example.batchordermanagement.integrations.batch.orders;

import com.example.batchordermanagement.orders.OrdersRepository;
import com.example.batchordermanagement.orders.order.Order;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.SkipListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.skip.AlwaysSkipItemSkipPolicy;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.RetryListener;
import org.springframework.retry.backoff.BackOffPolicy;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionOperations;

import java.time.Instant;
import java.util.Objects;

@Slf4j
@Component
@RequiredArgsConstructor
public class BatchOrderProcessingTemplates {

    private final TransactionOperations transactionOperations;
    private final OrdersRepository ordersRepository;
    private final OrderTracing orderTracing;
    private final StepBuilderFactory stepBuilderFactory;

    public ItemProcessorBuilder orderProcessor() {
        return new ItemProcessorBuilder();
    }

    public SkipListenerBuilder skipListener() {
        return new SkipListenerBuilder();
    }

    public StepTemplateBuilder step() {
        return new StepTemplateBuilder();
    }

    public BackOffPolicy fixedBackoff(long millis) {
        final FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(millis);
        return backOffPolicy;
    }

    public RetryListener retryListener() {
        return new RetryListener() {
            @Override
            public <T, E extends Throwable> boolean open(RetryContext context, RetryCallback<T, E> callback) {
                return true;
            }

            @Override
            public <T, E extends Throwable> void close(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {

            }

            @SneakyThrows
            @Override
            public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
                final String orderId = context.getAttribute(RetryContext.STATE_KEY).toString();
                orderTracing.traceOrder(orderId, () -> {
                    log.warn("Encountered an exception while processing item: {}. Reattempt of the operation is scheduled", orderId, throwable);
                    return null;
                });
            }
        };
    }

    public class StepTemplateBuilder {

        private String name;
        private ItemReader<String> reader;
        private ItemProcessor<String, Order> processor;
        private ItemWriter<Order> writer;
        private SkipListener<String, Order> skipListener;

        public StepTemplateBuilder name(String name) {
            this.name = name;
            return this;
        }

        public StepTemplateBuilder reader(ItemReader<String> reader) {
            this.reader = reader;
            return this;
        }

        public StepTemplateBuilder processor(ItemProcessor<String, Order> processor) {
            this.processor = processor;
            return this;
        }

        public StepTemplateBuilder writer(ItemWriter<Order> writer) {
            this.writer = writer;
            return this;
        }

        public StepTemplateBuilder skipListener(SkipListener<String, Order> skipListener) {
            this.skipListener = skipListener;
            return this;
        }

        public Step build() {
            Objects.requireNonNull(name);
            Objects.requireNonNull(reader);
            Objects.requireNonNull(processor);
            Objects.requireNonNull(writer);
            return stepBuilderFactory.get(name)
                    .allowStartIfComplete(true)
                    .<String, Order>chunk(5)
                    .reader(reader)
                    .processor(processor)
                    .writer(writer)
                    .faultTolerant()
                    .retryLimit(3)
                    .retry(Exception.class)
                    .backOffPolicy(
                            fixedBackoff(3_000L)
                    )
                    .skipPolicy(new AlwaysSkipItemSkipPolicy())
                    .listener(
                            retryListener()
                    )
                    .listener(skipListener)
                    .processorNonTransactional()
                    .build();
        }
    }

    public class SkipListenerBuilder {

        private String errorState;

        public SkipListenerBuilder errorState(String errorState) {
            this.errorState = errorState;
            return this;
        }

        public SkipListener<String, Order> build() {
            return new SkipListener<>() {
                @Override
                public void onSkipInRead(Throwable throwable) {
                }

                @Override
                public void onSkipInWrite(Order order, Throwable throwable) {
                }

                @Override
                @SneakyThrows
                public void onSkipInProcess(String orderId, Throwable throwable) {
                    orderTracing.traceOrder(orderId, () -> {
                        log.error("Skipping order: {} processing due to too many errors", orderId, throwable);
                        return transactionOperations.execute(status -> {
                            final Order order = ordersRepository.find(orderId);
                            ordersRepository.save(
                                    order.toBuilder()
                                            .state(errorState)
                                            .lastModified(Instant.now())
                                            .build()
                            );
                            return status;
                        });
                    });
                }
            };
        }
    }

    public class ItemProcessorBuilder {

        private String expectedState;
        private String lockedState;
        private OrderProcessorTemplate.CheckedFunction<Order, Order> invocation;

        public ItemProcessorBuilder expectedState(String expectedState) {
            this.expectedState = expectedState;
            return this;
        }

        public ItemProcessorBuilder lockedState(String lockedState) {
            this.lockedState = lockedState;
            return this;
        }

        public ItemProcessorBuilder invocation(OrderProcessorTemplate.CheckedFunction<Order, Order> invocation) {
            this.invocation = invocation;
            return this;
        }

        public ItemProcessor<String, Order> build() {
            Objects.requireNonNull(expectedState);
            Objects.requireNonNull(lockedState);
            Objects.requireNonNull(invocation);
            return new BatchOrderItemProcessor(
                    new OrderProcessorTemplate(transactionOperations, ordersRepository, expectedState, lockedState, orderTracing),
                    invocation
            );
        }
    }

    @RequiredArgsConstructor
    private static class BatchOrderItemProcessor implements ItemProcessor<String, Order> {

        private final OrderProcessorTemplate orderProcessorTemplate;
        private final OrderProcessorTemplate.CheckedFunction<Order, Order> delegate;

        @Override
        public Order process(String orderId) throws Exception {
            return orderProcessorTemplate.process(orderId, delegate);
        }
    }
}
