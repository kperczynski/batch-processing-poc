package com.example.batchordermanagement.integrations.batch.orders;

import com.example.batchordermanagement.integrations.batch.QOrders;
import com.example.batchordermanagement.orders.OrderStates;
import com.example.batchordermanagement.orders.OrdersRepository;
import com.example.batchordermanagement.orders.batch.BatchOrdersService;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Map;

@EnableBatchProcessing
public class BatchOrdersConfiguration {

    @Autowired
    private BatchOrdersService batchOrdersService;
    @Autowired
    private OrdersRepository ordersRepository;
    @Autowired
    private BatchOrdersWriter batchOrdersWriter;
    @Autowired
    private BatchOrderProcessingTemplates batchOrderProcessingTemplates;

    @Bean
    @QOrders
    public TaskExecutor taskExecutor() {
        final ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(2);
        taskExecutor.setMaxPoolSize(4);
        taskExecutor.setQueueCapacity(10);
        taskExecutor.setThreadNamePrefix("batch-job-executions");
        return taskExecutor;
    }

    @Bean
    @QOrders
    public Job ordersProcessingJob(JobBuilderFactory jobBuilderFactory,
                                   @Qualifier("commenceCustomerRegistrationStep") Step commenceCustomerRegistrationStep,
                                   @Qualifier("contractProductsStep") Step contractProductsStep) {
        return jobBuilderFactory.get("OrdersProcessingJob")
                .incrementer(new RunIdIncrementer())
                .start(commenceCustomerRegistrationStep)
                .next(contractProductsStep)
                .build();
    }

    @Bean
    public Step commenceCustomerRegistrationStep(@Qualifier("issuedOrdersReader") BatchOrdersReader idleOrdersReader) {
        return batchOrderProcessingTemplates.step()
                .name("CommenceCustomerRegistration")
                .reader(idleOrdersReader)
                .processor(
                        batchOrderProcessingTemplates.orderProcessor()
                                .expectedState(OrderStates.ISSUED)
                                .lockedState(OrderStates.CUSTOMER_REGISTRATION_COMMENCEMENT_IN_PROGRESS)
                                .invocation(batchOrdersService::commenceCustomerRegistration)
                                .build()
                )
                .writer(batchOrdersWriter)
                .skipListener(
                        batchOrderProcessingTemplates.skipListener()
                                .errorState(OrderStates.CUSTOMER_REGISTRATION_COMMENCEMENT_ERROR)
                                .build()
                )
                .build();
    }

    @Bean
    public Step contractProductsStep(@Qualifier("customerRegistrationCompletedOrdersReader") BatchOrdersReader customerRegistrationCompletedOrdersReader) {
        return batchOrderProcessingTemplates.step()
                .name("ContractProducts")
                .reader(customerRegistrationCompletedOrdersReader)
                .processor(
                        batchOrderProcessingTemplates.orderProcessor()
                                .expectedState(OrderStates.CUSTOMER_REGISTRATION_COMPLETED)
                                .lockedState(OrderStates.PRODUCTS_CONTRACTING_IN_PROGRESS)
                                .invocation(batchOrdersService::contractOrderedProducts)
                                .build()
                )
                .writer(batchOrdersWriter)
                .skipListener(
                        batchOrderProcessingTemplates.skipListener()
                                .errorState(OrderStates.PRODUCTS_CONTRACTING_ERROR)
                                .build()
                )
                .build();
    }

    @Bean
    @JobScope
    public BatchOrdersReader issuedOrdersReader(@VJobParameters Map<String, Object> jobParameters) {
        return new BatchOrdersReader(ordersRepository, OrderStates.ISSUED, jobParameters);
    }

    @Bean
    @JobScope
    public BatchOrdersReader customerRegistrationCompletedOrdersReader(@VJobParameters Map<String, Object> jobParameters) {
        return new BatchOrdersReader(ordersRepository, OrderStates.CUSTOMER_REGISTRATION_COMPLETED, jobParameters);
    }

    @Value("#{jobParameters}")
    @Retention(RetentionPolicy.RUNTIME)
    public @interface VJobParameters {
    }

}
