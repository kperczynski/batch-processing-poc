package com.example.batchordermanagement.integrations.persistence.orders;

import com.example.batchordermanagement.orders.order.OrderArtifacts;
import com.example.batchordermanagement.integrations.serialization.json.ObjectMappers;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.experimental.Delegate;

import java.util.HashMap;
import java.util.Map;

@Value
@AllArgsConstructor
class JsonOrderArtifacts implements OrderArtifacts {

    private static final TypeReference<Map<String, Object>> MAP_STRING_OBJECT_TYPEREF = new TypeReference<>() {
    };

    Map<String, JsonNode> artifacts;

    @Override
    public OrderArtifacts withArtifact(String key, Object value) {
        final JsonNode node = ObjectMappers.INSTANCE.convertValue(value, JsonNode.class);

        final Map<String, JsonNode> copiedArtifacts = new HashMap<>(artifacts);
        copiedArtifacts.put(key, node);
        return new JsonOrderArtifacts(copiedArtifacts);
    }

    @Override
    public <T> T findArtifact(String key, Class<T> clazz) {
        final JsonNode node = artifacts.get(key);
        if (node == null) {
            return null;
        }
        return ObjectMappers.INSTANCE.convertValue(node, clazz);
    }

    @Override
    public Map<String, Object> toMap() {
        return ObjectMappers.INSTANCE.convertValue(artifacts, MAP_STRING_OBJECT_TYPEREF);
    }

}
