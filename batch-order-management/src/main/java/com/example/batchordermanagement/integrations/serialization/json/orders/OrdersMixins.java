package com.example.batchordermanagement.integrations.serialization.json.orders;

import com.example.batchordermanagement.orders.order.IncomingCustomer;
import com.example.batchordermanagement.orders.order.ProductOffering;
import com.example.batchordermanagement.orders.products.ContractedProducts;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

import static com.example.batchordermanagement.orders.products.ContractedProducts.*;

public class OrdersMixins {

    public static class OrderFormMixin {
        @JsonCreator
        public OrderFormMixin(@JsonProperty("customer") IncomingCustomer customer,
                              @JsonProperty("products") List<ProductOffering> products) {
        }
    }

    public static class ProductOfferingMixin {
        @JsonCreator
        public ProductOfferingMixin(@JsonProperty("productId") String productId,
                                    @JsonProperty("attributes") Map<String, String> attributes) {
        }
    }

    public static class IncomingCustomerMixin {
        @JsonCreator
        public IncomingCustomerMixin(
                @JsonProperty("firstName") String firstName,
                @JsonProperty("lastName") String lastName,
                @JsonProperty("oib") String oib) {
        }
    }

    public static class ContractedProductsMixin {
        @JsonCreator
        public ContractedProductsMixin(@JsonProperty("contractedProducts") List<ContractedProduct> contractedProducts) {
        }
    }

    public static class ContractedProductMixin {
        @JsonCreator
        public ContractedProductMixin(@JsonProperty("activationId") String activationId,
                                      @JsonProperty("productOffering") ProductOffering productOffering) {
        }
    }

}
