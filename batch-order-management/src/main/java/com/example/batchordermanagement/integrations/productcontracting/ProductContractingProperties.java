package com.example.batchordermanagement.integrations.productcontracting;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.net.URI;

@Data
@ConfigurationProperties("product-contracting")
public class ProductContractingProperties {

    URI serviceUrl;

}
