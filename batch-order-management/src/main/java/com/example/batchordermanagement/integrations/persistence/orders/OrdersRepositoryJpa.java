package com.example.batchordermanagement.integrations.persistence.orders;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.lang.Nullable;

import javax.persistence.LockModeType;
import java.util.List;

interface OrdersRepositoryJpa extends JpaRepository<OrderEntity, String> {

    @Query("select o from OrderEntity o where o.state = :state order by o.lastModified desc")
    List<OrderEntity> findAllByState(String state, Pageable pageable);

    @Query("select o from OrderEntity o where o.id = :id and o.state = :state")
    @Nullable
    OrderEntity findByIdAndState(String id, String state);

    @Nullable
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("select o from OrderEntity o where o.id = :orderId")
    OrderEntity selectForUpdate(String orderId);

}
