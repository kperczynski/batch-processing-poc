package com.example.batchordermanagement.integrations.batch.orders;

import com.example.batchordermanagement.orders.OrdersRepository;
import com.example.batchordermanagement.orders.order.Order;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionOperations;

import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class BatchOrdersWriter implements ItemWriter<Order> {

    private final TransactionOperations transactionOperations;
    private final OrdersRepository ordersRepository;

    @Override
    public void write(List<? extends Order> items) throws Exception {
        transactionOperations.execute(status -> {
            for (Order order : items) {
                ordersRepository.save(order);
            }
            return status;
        });
    }
}
