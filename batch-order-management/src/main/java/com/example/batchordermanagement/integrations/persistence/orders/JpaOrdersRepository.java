package com.example.batchordermanagement.integrations.persistence.orders;

import com.example.batchordermanagement.orders.order.Order;
import com.example.batchordermanagement.orders.order.OrderForm;
import com.example.batchordermanagement.orders.OrdersRepository;
import com.example.batchordermanagement.integrations.serialization.json.ObjectMappers;
import com.example.batchprocessing.exceptions.ResourceDoesNotExistException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.domain.Pageable;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class JpaOrdersRepository implements OrdersRepository {

    private static final TypeReference<Map<String, JsonNode>> ARTIFACTS_TYPEREF = new TypeReference<>() {
    };

    private final OrdersRepositoryJpa ordersRepositoryJpa;

    @Override
    public List<Order> findAllOrdersInState(String state, Pageable pageable) {
        final List<OrderEntity> orders = ordersRepositoryJpa.findAllByState(state, pageable);

        return orders.stream()
                .map(JpaOrdersRepository::toOrder)
                .collect(Collectors.toList());
    }

    @Override
    public Order find(String id) {
        final OrderEntity order = ordersRepositoryJpa.findById(id)
                .orElseThrow(() -> new ResourceDoesNotExistException("order", id));
        return toOrder(order);
    }

    @Override
    @Nullable
    public Order tryFindOrderInState(String id, String state) {
        final OrderEntity order = ordersRepositoryJpa.findByIdAndState(id, state);
        return order != null
                ? toOrder(order)
                : null;
    }

    @Override
    @Nullable
    public Order trySelectForUpdate(String id) {
        final OrderEntity orderEntity = ordersRepositoryJpa.selectForUpdate(id);
        if (orderEntity == null) {
            return null;
        }
        return toOrder(orderEntity);
    }

    @Override
    public Order save(Order order) {
        final OrderEntity orderEntity = toOrderEntity(order);
        ordersRepositoryJpa.save(orderEntity);
        return order;
    }

    @SneakyThrows
    private static Order toOrder(OrderEntity orderEntity) {
        final OrderForm orderForm = ObjectMappers.INSTANCE.readValue(orderEntity.getForm(), OrderForm.class);
        final Map<String, JsonNode> artifacts = ObjectMappers.INSTANCE.readValue(
                orderEntity.getArtifacts(),
                ARTIFACTS_TYPEREF
        );

        return Order.builder()
                .id(orderEntity.getId())
                .state(orderEntity.getState())
                .artifacts(new JsonOrderArtifacts(artifacts))
                .form(orderForm)
                .createdAt(orderEntity.getCreatedAt())
                .lastModified(orderEntity.getLastModified())
                .build();
    }


    @SneakyThrows
    private static OrderEntity toOrderEntity(Order order) {
        final String orderForm = ObjectMappers.INSTANCE.writeValueAsString(order.getForm());
        final String artifacts = ObjectMappers.INSTANCE.writeValueAsString(order.getArtifacts().toMap());

        return OrderEntity.builder()
                .id(order.getId())
                .state(order.getState())
                .form(orderForm)
                .artifacts(artifacts)
                .createdAt(order.getCreatedAt())
                .lastModified(order.getLastModified())
                .build();
    }
}
