package com.example.batchordermanagement.integrations.customersmanagement;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;

import java.net.http.HttpClient;

@EnableConfigurationProperties(CustomersManagementProperties.class)
@EnableBinding(CustomerManagementEventsStream.class)
public class CustomersManagementConfiguration {

    @Bean
    @QCustomersManagement
    public HttpClient customersManagementHttpClient() {
        return HttpClient.newHttpClient();
    }

}
