package com.example.batchordermanagement.integrations.batch.orders;

import brave.Span;
import brave.Tracer;
import brave.propagation.TraceContext;
import lombok.RequiredArgsConstructor;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class OrderTracing {

    private final Tracer tracer;

    public <T> T traceOrder(String orderId, CheckedSupplier<T> supplier) throws Exception {
        final Span span = tracer.newChild(
                TraceContext.newBuilder()
                        .traceId(UUID.fromString(orderId).getLeastSignificantBits())
                        .spanId(Math.abs(new Random().nextLong()))
                        .build()
        );
        try (final Tracer.SpanInScope spanInScope = tracer.withSpanInScope(span.start())) {
            return supplier.supply();
        }
        finally {
            span.finish();
            MDC.clear();
        }
    }

    public interface CheckedSupplier<T> {
        T supply() throws Exception;
    }
}
