package com.example.batchordermanagement.integrations.batch;

import com.example.batchordermanagement.integrations.batch.orders.BatchOrdersReader;
import com.example.batchordermanagement.orders.OrderProcessingJobLauncher;
import com.example.batchordermanagement.orders.order.Order;
import com.example.batchprocessing.exceptions.UserErrorException;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

@Component
public class AsyncOrderProcessingJobLauncher implements OrderProcessingJobLauncher {

    private final TaskExecutor taskExecutor;
    private final Job job;
    private final JobLauncher jobLauncher;

    public AsyncOrderProcessingJobLauncher(@QOrders TaskExecutor taskExecutor,
                                           @QOrders Job job,
                                           JobLauncher jobLauncher) {
        this.taskExecutor = taskExecutor;
        this.job = job;
        this.jobLauncher = jobLauncher;
    }

    @Override
    public void runOrderProcessing(Order order) {
        taskExecutor.execute(() -> {
            try {
                jobLauncher.run(
                        job,
                        new JobParametersBuilder()
                                .addLong("run.id", System.nanoTime())
                                .addString(BatchOrdersReader.ORDER_ID_PARAMETER, order.getId())
                                .toJobParameters()
                );
            } catch (JobExecutionAlreadyRunningException e) {
                throw new UserErrorException("job-already-running", e);
            } catch (JobRestartException e) {
                throw new UserErrorException("job-restart", e);
            } catch (JobInstanceAlreadyCompleteException e) {
                throw new UserErrorException("job-already-completed", e);
            } catch (JobParametersInvalidException e) {
                throw new UserErrorException("invalid-job-parameters", e);
            }
        });
    }

    @Override
    public void runAllOrdersProcessing() {
        taskExecutor.execute(() -> {
            try {
                jobLauncher.run(job, new JobParameters());
            } catch (JobExecutionAlreadyRunningException e) {
                throw new UserErrorException("job-already-running", e);
            } catch (JobRestartException e) {
                throw new UserErrorException("job-restart", e);
            } catch (JobInstanceAlreadyCompleteException e) {
                throw new UserErrorException("job-already-completed", e);
            } catch (JobParametersInvalidException e) {
                throw new UserErrorException("invalid-job-parameters", e);
            }
        });
    }
}
