package com.example.batchordermanagement.integrations.serialization.json;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import static com.example.batchordermanagement.integrations.serialization.json.orders.OrdersJacksonModule.ordersJacksonModule;

public class ObjectMappers {

    public static final ObjectMapper INSTANCE;

    static {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModules(new JavaTimeModule());
        objectMapper.registerModules(ordersJacksonModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        INSTANCE = objectMapper;
    }

}
