package com.example.batchordermanagement.integrations.productcontracting;

import com.example.batchordermanagement.integrations.serialization.json.ObjectMappers;
import com.example.batchordermanagement.orders.order.ProductOffering;
import com.example.batchordermanagement.orders.products.ProductActivation;
import com.example.batchordermanagement.orders.products.ProductContractingClient;
import com.example.batchordermanagement.orders.products.ProductContractingClientException;
import com.example.batchprocessing.tracing.HttpClientTracing;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;
import java.util.Objects;

import static com.example.batchprocessing.correlation.CorrelationIdContext.getCurrentCorrelationId;
import static com.example.batchprocessing.logging.HttpClientLogs.logRequest;
import static com.example.batchprocessing.logging.HttpClientLogs.logResponse;

@Slf4j
@Component
public class HttpProductContractingClient implements ProductContractingClient {

    private final ProductContractingProperties productContractingProperties;
    private final HttpClientTracing httpClientTracing;
    private final HttpClient httpClient;

    public HttpProductContractingClient(ProductContractingProperties productContractingProperties,
                                        HttpClientTracing httpClientTracing,
                                        @QProductContracting HttpClient httpClient) {
        this.productContractingProperties = productContractingProperties;
        this.httpClientTracing = httpClientTracing;
        this.httpClient = httpClient;
    }

    @Override
    public ProductActivation contractProduct(String clientId, ProductOffering productOffering) {
        final String correlationId = Objects.requireNonNull(
                getCurrentCorrelationId(),
                "Missing correlation id for customer registration"
        );
        final URI uri = UriComponentsBuilder.fromUri(productContractingProperties.getServiceUrl())
                .path("/product-activations")
                .build(Map.of());
        try {
            final HttpRequest request = httpClientTracing.tracedHttpRequest()
                    .PUT(HttpRequest.BodyPublishers.ofString(
                            toJson(
                                    JsonNodeFactory.instance.objectNode()
                                            .put("clientId", clientId)
                                            .put("productId", productOffering.getProductId())
                                            .putPOJO("attributes", productOffering.getAttributes())
                            )
                    ))
                    .uri(uri)
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .header("X-Correlation-Id", correlationId)
                    .build();

            logRequest(log, request);
            final HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            logResponse(log, response);

            if (HttpStatus.valueOf(response.statusCode()).is2xxSuccessful()) {
                final JsonNode node = ObjectMappers.INSTANCE.readValue(response.body(), JsonNode.class);
                return new ProductActivation(node.get("id").asText());
            }
            throw new ProductContractingClientException("Unexpected response status: " + response.statusCode());
        } catch (IOException e) {
            throw new ProductContractingClientException("Unexpected transport layer exception", e);
        } catch (InterruptedException e) {
            throw gracefullyHandleInterruptedException(e);
        }
    }

    @SneakyThrows
    private static RuntimeException gracefullyHandleInterruptedException(InterruptedException e) {
        throw e;
    }

    @SneakyThrows
    private static String toJson(Object object) {
        return ObjectMappers.INSTANCE.writeValueAsString(object);
    }

}
