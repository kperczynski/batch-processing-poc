package com.example.batchordermanagement.integrations.customersmanagement;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class CustomerRegistrationProcessDto {

    String id;
    String state;
    CustomerDto customer;

    @JsonCreator
    public CustomerRegistrationProcessDto(@JsonProperty("id") String id,
                                          @JsonProperty("state") String state,
                                          @JsonProperty("customer") CustomerDto customer) {
        this.id = id;
        this.state = state;
        this.customer = customer;
    }

    @Value
    public static class CustomerDto {

        String id;
        String firstName;
        String lastName;
        String oib;

        @JsonCreator
        public CustomerDto(@JsonProperty("id") String id,
                           @JsonProperty("firstName") String firstName,
                           @JsonProperty("lastName") String lastName,
                           @JsonProperty("oib") String oib) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.oib = oib;
        }
    }

}
