package com.example.batchordermanagement.integrations.productcontracting;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import java.net.http.HttpClient;

@EnableConfigurationProperties(ProductContractingProperties.class)
public class ProductContractingConfiguration {

    @Bean
    @QProductContracting
    public HttpClient httpClient() {
        return HttpClient.newHttpClient();
    }

}
