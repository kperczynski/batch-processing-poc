package com.example.batchordermanagement.integrations.serialization.json.orders;

import com.example.batchordermanagement.orders.order.IncomingCustomer;
import com.example.batchordermanagement.orders.order.OrderForm;
import com.example.batchordermanagement.orders.order.ProductOffering;
import com.example.batchordermanagement.orders.products.ContractedProducts;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class OrdersJacksonModule {

    public static SimpleModule ordersJacksonModule() {
        final SimpleModule module = new SimpleModule("OrdersJacksonModule");
        module.setMixInAnnotation(IncomingCustomer.class, OrdersMixins.IncomingCustomerMixin.class);
        module.setMixInAnnotation(ProductOffering.class, OrdersMixins.ProductOfferingMixin.class);
        module.setMixInAnnotation(OrderForm.class, OrdersMixins.OrderFormMixin.class);
        module.setMixInAnnotation(ContractedProducts.ContractedProduct.class, OrdersMixins.ContractedProductMixin.class);
        module.setMixInAnnotation(ContractedProducts.class, OrdersMixins.ContractedProductsMixin.class);
        return module;
    }

}
