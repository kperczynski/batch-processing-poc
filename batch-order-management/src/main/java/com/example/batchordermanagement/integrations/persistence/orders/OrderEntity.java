package com.example.batchordermanagement.integrations.persistence.orders;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.util.Objects;

@Entity
@Data
@Table(name = "processed_order")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderEntity {

    @Id
    private String id;

    @Column(nullable = false, length = 4096)
    private String form;

    @Column(nullable = false, length = 4096)
    private String artifacts;

    @Column(nullable = false)
    private String state;

    @Column(nullable = false)
    private Instant createdAt;

    @Column(nullable = false)
    private Instant lastModified;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderEntity)) return false;
        OrderEntity that = (OrderEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
