package com.example.batchordermanagement.integrations.batch;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.function.Supplier;

@Slf4j
@RequiredArgsConstructor
public class QueryingItemReader<T> implements ItemReader<T>, ItemStream {

    private final Supplier<List<T>> queryFunction;

    private Queue<T> queue = new LinkedList<>();

    @Override
    public T read() {
        return queue.poll();
    }

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {
        final List<T> items = queryFunction.get();
        queue = new LinkedList<>(items);
    }

    @Override
    public void close() throws ItemStreamException {
    }
}
