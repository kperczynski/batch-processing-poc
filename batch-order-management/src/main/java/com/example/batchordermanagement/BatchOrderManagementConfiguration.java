package com.example.batchordermanagement;

import com.example.batchordermanagement.integrations.batch.orders.BatchOrdersConfiguration;
import com.example.batchordermanagement.integrations.customersmanagement.CustomersManagementConfiguration;
import com.example.batchordermanagement.integrations.productcontracting.ProductContractingConfiguration;
import com.example.batchordermanagement.integrations.serialization.json.ObjectMappers;
import com.example.batchordermanagement.orders.OrderProcessingJob;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.util.DynamicPeriodicTrigger;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.time.Duration;

@Configuration
@Import({
        BatchOrdersConfiguration.class,
        CustomersManagementConfiguration.class,
        ProductContractingConfiguration.class
})
@EnableScheduling
public class BatchOrderManagementConfiguration {

    @Bean
    public SchedulingConfigurer schedulingConfigurer(OrderProcessingJob orderProcessingJob) {
        return new SchedulingConfigurer() {
            @Override
            public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
                scheduledTaskRegistrar.addTriggerTask(
                        orderProcessingJob::runAllOrdersProcessing,
                        everyHour(randomDelay())
                );
            }
        };
    }

    @Bean
    public ObjectMapper objectMapper() {
        return ObjectMappers.INSTANCE;
    }

    private static Duration randomDelay() {
        final long secondsOfDelays = (long) (Math.random() * 15 + 5);
        return Duration.ofSeconds(secondsOfDelays);
    }

    private static DynamicPeriodicTrigger everyHour(Duration delay) {
        final DynamicPeriodicTrigger trigger = new DynamicPeriodicTrigger(Duration.ofHours(1L));
        trigger.setInitialDuration(delay);
        return trigger;
    }
}
