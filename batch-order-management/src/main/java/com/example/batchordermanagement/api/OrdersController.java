package com.example.batchordermanagement.api;

import com.example.batchordermanagement.orders.OrderService;
import com.example.batchordermanagement.orders.order.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.List;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class OrdersController {

    private final OrderService orderService;

    @GetMapping("/orders/{orderId}")
    public ResponseEntity<?> readOrder(@PathVariable("orderId") String orderId) {
        final Order order = orderService.readOrder(orderId);
        return ResponseEntity.ok(
                toReadOrderResponse(order)
        );
    }

    @PostMapping("/orders")
    public ResponseEntity<?> readOrder(@RequestBody CreateOrderRequest createOrderRequest) {
        final Order order = orderService.createOrder(
                OrderForm.builder()
                        .customer(createOrderRequest.getCustomer())
                        .products(createOrderRequest.getProducts())
                        .build()
        );
        return ResponseEntity.ok(order);
    }

    private static ReadOrderResponse toReadOrderResponse(Order order) {
        return ReadOrderResponse.builder()
                .id(order.getId())
                .state(order.getState())
                .artifacts(order.getArtifacts().toMap())
                .createdAt(order.getCreatedAt())
                .lastModified(order.getLastModified())
                .form(order.getForm())
                .build();
    }

    @Value
    public static class CreateOrderRequest {

        @NotNull
        IncomingCustomer customer;
        @NotNull
        List<ProductOffering> products;

        @JsonCreator
        public CreateOrderRequest(@JsonProperty("customer") IncomingCustomer customer,
                                  @JsonProperty("products") List<ProductOffering> products) {
            this.customer = customer;
            this.products = products;
        }
    }

    @Value
    @Builder
    public static class ReadOrderResponse {


        String id;
        String state;
        OrderForm form;
        Map<String, Object> artifacts;
        Instant createdAt;
        Instant lastModified;

    }
}
