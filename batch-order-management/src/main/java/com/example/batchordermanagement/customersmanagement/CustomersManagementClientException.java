package com.example.batchordermanagement.customersmanagement;

public class CustomersManagementClientException extends RuntimeException {

    public CustomersManagementClientException(String message) {
        super(message);
    }

    public CustomersManagementClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
