package com.example.batchordermanagement.customersmanagement;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Customer {

    String id;
    String firstName;
    String lastName;
    String oib;

}
