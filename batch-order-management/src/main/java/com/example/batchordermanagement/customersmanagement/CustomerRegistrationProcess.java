package com.example.batchordermanagement.customersmanagement;

import lombok.Builder;
import lombok.Value;
import org.springframework.lang.Nullable;

@Value
@Builder
public class CustomerRegistrationProcess {
    String id;
    String state;
    @Nullable
    Customer registeredCustomer;
}
