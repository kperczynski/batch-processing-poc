package com.example.batchordermanagement.customersmanagement;

import com.example.batchordermanagement.orders.order.IncomingCustomer;

public interface CustomersManagementClient {

    CustomerRegistrationProcess commenceCustomerRegistration(IncomingCustomer incomingCustomer);

}
