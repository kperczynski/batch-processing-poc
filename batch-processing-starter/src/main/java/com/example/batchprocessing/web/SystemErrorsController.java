package com.example.batchprocessing.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.RequestDispatcher;
import java.util.Map;

@Slf4j
@Controller
@RequestMapping("${server.error.path:${error.path:/error}}")
public class SystemErrorsController implements ErrorController {

    private final ErrorAttributes errorAttributes;

    public SystemErrorsController(ErrorAttributes errorAttributes) {
        this.errorAttributes = errorAttributes;
    }

    @RequestMapping
    public ResponseEntity<?> handleStatusNotFound(WebRequest request) {
        final Integer status = (Integer) request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE, WebRequest.SCOPE_REQUEST);
        if (status == null) {
            return handleInternalServerError(errorAttributes.getError(request));
        }
        switch (status) {
            case 404:
                return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body(Map.of("code", "endpoint-not-found"));
            case 503:
                return ResponseEntity
                        .status(HttpStatus.SERVICE_UNAVAILABLE)
                        .body(Map.of("code", "service-unavailable"));
            case 500:
            default:
                return handleInternalServerError(errorAttributes.getError(request));
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public String getErrorPath() {
        return null;
    }


    private static ResponseEntity<Map<String, String>> handleInternalServerError(@Nullable Throwable error) {
        log.error("Unexpected exception has occurred. This incident should be reported", error);
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(Map.of("code", "internal-server-error"));
    }
}
