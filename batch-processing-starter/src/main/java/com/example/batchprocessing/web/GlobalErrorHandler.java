package com.example.batchprocessing.web;

import com.example.batchprocessing.exceptions.ResourceDoesNotExistException;
import com.example.batchprocessing.exceptions.UserErrorException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

@RestControllerAdvice
public class GlobalErrorHandler {

    @ExceptionHandler(UserErrorException.class)
    public ResponseEntity<?> handleUserError(UserErrorException e) {
        return ResponseEntity.unprocessableEntity()
                .body(Map.of("code", e.getCode()));
    }

    @ExceptionHandler(ResourceDoesNotExistException.class)
    public ResponseEntity<?> handleUserError(ResourceDoesNotExistException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Map.of("code", "resource-does-not-exist", e.getIdentifierType(), e.getId()));
    }

}
