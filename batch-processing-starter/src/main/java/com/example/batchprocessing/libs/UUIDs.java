package com.example.batchprocessing.libs;

import java.util.UUID;

public class UUIDs {
    public static String nextUuid() {
        return UUID.randomUUID().toString();
    }
}
