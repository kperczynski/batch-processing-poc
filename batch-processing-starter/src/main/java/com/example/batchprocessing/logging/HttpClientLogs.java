package com.example.batchprocessing.logging;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Set;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

@Slf4j
@UtilityClass
public class HttpClientLogs {

    private static final List<String> MASKED_HEADER_VALUE = List.of("***");
    private static final Set<MediaType> LOGGABLE_FORMATS = Set.of(
            MediaType.APPLICATION_JSON,
            MediaType.APPLICATION_XML,
            MediaType.TEXT_HTML,
            MediaType.TEXT_PLAIN
    );

    public static void logRequest(Logger log, HttpRequest request) {
        final StringBuilder sink = new StringBuilder();

        sink.append(String.format("Making request to: %s %s", request.method(), request.uri())).append('\n');
        logHeaders(sink, request.headers());

        if (request.bodyPublisher().isEmpty()) {
            log.info(sink.toString());
            return;
        }

        final MediaType mediaType = findBodyContentType(request.headers());
        if (!formatIsAvailableForLogging(mediaType)) {
            sink.append("Body: Binary content or format is not loggable").append('\n');
            return;
        }

        request.bodyPublisher().orElseThrow()
                .subscribe(new LoggingSubsriber(log, sink));
    }

    public static void logResponse(Logger log, HttpResponse<?> response) {
        final StringBuilder sink = new StringBuilder();

        sink.append(String.format("Response from: %s %s", response.request().method(), response.uri())).append('\n');
        sink.append(String.format("Status code: %s", HttpStatus.valueOf(response.statusCode()))).append('\n');
        logHeaders(sink, response.headers());

        final Object body = response.body();
        if (body == null) {
            log.info(sink.toString());
            return;
        }

        final MediaType mediaType = findBodyContentType(response.headers());
        if (!formatIsAvailableForLogging(mediaType)) {
            sink.append("Body: Binary content or format is not loggable").append('\n');
            log.info(sink.toString());
            return;
        }

        sink.append("Body: \n").append(response.body().toString());
        log.info(sink.toString());
    }

    private static MediaType findBodyContentType(HttpHeaders headers) {
        return headers.firstValue(CONTENT_TYPE)
                .map(MediaType::parseMediaType)
                .orElse(MediaType.APPLICATION_OCTET_STREAM);
    }

    private static void logHeaders(StringBuilder sink, HttpHeaders headers) {
        sink.append("Headers:").append('\n');
        for (var entry : headers.map().entrySet()) {
            final String header = entry.getKey();
            final List<String> values = isSensitiveHeader(header)
                    ? MASKED_HEADER_VALUE
                    : entry.getValue();
            sink.append(String.format("  %s: %s", header, values)).append('\n');
        }
    }

    private static boolean formatIsAvailableForLogging(MediaType mediaType) {
        return LOGGABLE_FORMATS.stream()
                .anyMatch(mediaType::isCompatibleWith);
    }

    private static boolean isSensitiveHeader(String header) {
        return header.equalsIgnoreCase(AUTHORIZATION);
    }

}
