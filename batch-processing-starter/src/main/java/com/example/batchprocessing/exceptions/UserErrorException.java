package com.example.batchprocessing.exceptions;

import lombok.Getter;

@Getter
public class UserErrorException extends RuntimeException {

    private final String code;

    public UserErrorException(String code) {
        super("Encountered user error: " + code);
        this.code = code;
    }

    public UserErrorException(String code, Exception e) {
        super("Encountered user error: " + code, e);
        this.code = code;
    }
}
