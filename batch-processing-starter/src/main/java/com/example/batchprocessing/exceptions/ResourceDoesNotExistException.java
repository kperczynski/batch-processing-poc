package com.example.batchprocessing.exceptions;

import lombok.Getter;

@Getter
public class ResourceDoesNotExistException extends RuntimeException {

    private final String resource;
    private final String id;
    private final String identifierType;

    public ResourceDoesNotExistException(String resource, String id) {
        this(resource, id, "id");
    }

    public ResourceDoesNotExistException(String resource, String id, String identifierType) {
        super("Cannot find resource: " + resource + " with " + identifierType + ": " + id);
        this.resource = resource;
        this.id = id;
        this.identifierType = identifierType;
    }
}
