package com.example.batchprocessing;

import brave.http.HttpTracing;
import com.example.batchprocessing.tracing.HttpClientTracing;
import com.example.batchprocessing.correlation.CorrelationIdInterceptor;
import com.example.batchprocessing.web.GlobalErrorHandler;
import com.example.batchprocessing.web.SystemErrorsController;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import static org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication.Type;

@Configuration
@ConditionalOnWebApplication(type = Type.SERVLET)
@AutoConfigureBefore(ErrorMvcAutoConfiguration.class)
@ConditionalOnProperty(value = "batch-processing.autoconfigure", matchIfMissing = true)
public class BatchProcessingStarterAutoConfiguration {

    @Bean
    public ErrorController systemErrorsController(ErrorAttributes errorAttributes) {
        return new SystemErrorsController(errorAttributes);
    }

    @Bean
    public GlobalErrorHandler globalErrorHandler() {
        return new GlobalErrorHandler();
    }

    @Bean
    public CorrelationIdInterceptor correlationIdInterceptor() {
        return new CorrelationIdInterceptor();
    }

    @Bean
    public WebMvcConfigurer webMvcConfigurer(CorrelationIdInterceptor correlationIdInterceptor) {
        return new WebMvcConfigurer() {
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(correlationIdInterceptor);
            }
        };
    }

    @Bean
    public HttpClientTracing httpClientTracing(HttpTracing httpTracing) {
        return new HttpClientTracing(httpTracing);
    }

}
