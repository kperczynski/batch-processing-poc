package com.example.batchprocessing.tracing;

import brave.Span;
import brave.http.HttpTracing;
import brave.propagation.TraceContext;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.net.http.HttpRequest;

@AllArgsConstructor
public class HttpClientTracing {

    private final HttpTracing httpTracing;

    public HttpRequest.Builder tracedHttpRequest() {
        final TraceContext.Injector<HttpRequest.Builder> injector = httpTracing.tracing().propagation().injector(HttpRequest.Builder::header);
        final Span span = httpTracing.tracing().tracer().nextSpan();
        final HttpRequest.Builder builder = HttpRequest.newBuilder();
        injector.inject(span.context(), builder);
        return builder;
    }

}