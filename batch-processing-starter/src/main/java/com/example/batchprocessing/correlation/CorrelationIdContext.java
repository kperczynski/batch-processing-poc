package com.example.batchprocessing.correlation;

import org.slf4j.MDC;
import org.springframework.lang.Nullable;

public class CorrelationIdContext {

    public static final String CORRELATION_ID = "correlationId";

    @Nullable
    public static String getCurrentCorrelationId() {
        return MDC.get(CORRELATION_ID);
    }

    public static void setCurrentCorrelationId(String correlationId) {
        MDC.put(CORRELATION_ID, correlationId);
    }

}
