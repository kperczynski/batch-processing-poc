package com.example.batchprocessing.correlation;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class CorrelationIdInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        log.trace("Intercepting request to: {} with CorrelationIdInterceptor", request.getRequestURL());
        final String correlationId = request.getHeader("X-Correlation-Id");
        MDC.put(CorrelationIdContext.CORRELATION_ID, correlationId);
        return true;
    }
}
