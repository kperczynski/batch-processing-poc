# Batch Processing PoC

## Building the project & execution

| Action              | Command                          |
| ------------------- | -------------------------------- |
| Project compilation | `mvnw clean install -DskipTests` |
| Building docker images | `docker-compose -f docker-compose.build.yaml build` |
| Running all applications in "cluster" | `docker-compose up -d` |

## System components & architecture

The presented system consists of 3 applications:

1. batch-order-processing
2. batch-customers-management
3. batch-products-contracting

### Responsibilities

| Application                | Responsibility                 |
| -------------------------- | ------------------------------ |
| batch-order-processing     | Order processing orchestration |
| batch-customers-management | Creating customers             |
| batch-products-contracting | Contracting products           |

After creating an order in `batch-orders-processing` it gets process throughout the system. The usual flow consists of following phases:

1. Order gets picked up by the system
2. Customer gets created as an asynchronous operation
3. Products get contracted as multiple invocations of the REST endpoint

### System components diagram

```plantuml
@startuml
interface HTTP
queue Kafka

artifact BatchOrderProcessing
artifact BatchCustomersManagement
artifact BatchProductsContracting

HTTP -(0- BatchOrderProcessing

BatchOrderProcessing - Kafka
BatchCustomersManagement - Kafka

BatchOrderProcessing -(0- BatchCustomersManagement: HTTP
BatchOrderProcessing -(0- BatchProductsContracting: HTTP
@enduml
```

### Order processing state diagram

```plantuml
@startuml
hide empty description

[*] --> Issued

Issued --> CustomerRegistration

state CustomerRegistration {
  CustomerRegistrationCommencementCompleted: The system awaits for the async notification with the result
  [*] --> CustomerRegistrationCommencementInProgress
  CustomerRegistrationCommencementInProgress --> CustomerRegistrationCommencementError
  CustomerRegistrationCommencementInProgress --> CustomerRegistrationCommencementCompleted
  CustomerRegistrationCommencementError --> [*]
  CustomerRegistrationCommencementCompleted --> CustomerRegistrationCompleted 
  CustomerRegistrationCommencementCompleted --> CustomerRegistrationError
  CustomerRegistrationError --> [*]
}


CustomerRegistrationCompleted --> ProductsContracting

state ProductsContracting {
  [*] --> ProductsContractingInProgress
  ProductsContractingInProgress --> ProductsContractingCompleted
  ProductsContractingInProgress --> ProductsContractingPartiallyCompleted
  ProductsContractingInProgress --> ProductsContractingError
  ProductsContractingError --> [*]
  ProductsContractingCompleted --> [*]
  ProductsContractingPartiallyCompleted --> [*]
}
@enduml
```

## Order model

| Field        | Type           | Description                                                  |
| ------------ | -------------- | ------------------------------------------------------------ |
| id           | String         | self-explainatory                                            |
| state        | String         | self-explainatory                                            |
| orderForm    | OrderForm      | Immutable sub-document that contains the order issued by the purchaser. Includes customer data and ordered products. |
| artifacts    | OrderArtifacts | A map of documents containing all items created or acquired during processing an order e.g. registered customer id |
| createdAt    | Instant        | self-explainatory                                            |
| lastModified | Instant        | self-explainatory                                            |

### Example OrderForm

```json
{
  "customer": {
    "firstName": "John",
    "lastName": "Doe",
    "oib": "230948234"
  },
  "products": [
    {
      "productId": "DEBIT_CARD",
      "attributes": {
        "CARDHOLDER_NAME": "JOHN DOE"
      }
    },
    {
      "productId": "PERSONAL_BANK_ACCOUNT",
      "attributes": {}
    }
  ]
}
```

### Example processed order

```json
{
  "id": "0b615684-7ced-4348-8900-eb9a7883c9a2",
  "state": "PRODUCTS_CONTRACTING_COMPLETED",
  "form": {
    "customer": {
      "firstName": "John",
      "lastName": "Doe",
      "oib": "230948234"
    },
    "products": [
      {
        "productId": "DEBIT_CARD",
        "attributes": {
          "CARDHOLDER_NAME": "JOHN DOE"
        }
      },
      {
        "productId": "PERSONAL_BANK_ACCOUNT",
        "attributes": {}
      }
    ]
  },
  "artifacts": {
    "CUSTOMER_REGISTRATION_PROCESS_ID": "6aa94c2a-feef-4de4-b88a-4c42e7406bfb",
    "REGISTERED_CUSTOMER_ID": "bb29a1ad-eb27-4cd3-a080-21d81635c913",
    "CONTRACTED_PRODUCTS": {
      "contractedProducts": [
        {
          "activationId": "ffc1c9b5-9577-4e88-adab-1a4098ddcf5c",
          "productOffering": {
            "productId": "DEBIT_CARD",
            "attributes": {
              "CARDHOLDER_NAME": "JOHN DOE"
            }
          }
        },
        {
          "activationId": "237fa781-85f0-4f4a-afa9-23998cad5dfc",
          "productOffering": {
            "productId": "PERSONAL_BANK_ACCOUNT",
            "attributes": {}
          }
        }
      ]
    }
  },
  "createdAt": "2021-02-04T09:38:30.569541Z",
  "lastModified": "2021-02-04T09:38:36.937426Z"
}
```

## Batch Processing

To organize the executions of the consecutive phases, `batch-order-management` component uses Spring Batch framework: https://docs.spring.io/spring-batch/docs/current/reference/html/index.html.

### Integration

Order processing consists into 2 phases that are executed in `batch-order-management` and so they have corresponding `Step`, `ItemReader` definitions.

Usual `Step` configuration works as following:

1.  Item reader reads orders in state e.g. `CUSTOMER_REGISTRATION_COMPLETED`

2. Orders batch is passed into `ItemProcessor` that sequentially executes the step logic:

   2.1. Locking an item in state `PRODUCTS_CONTRACTING_IN_PROGRESS`

   2.2. Executing the actual logic that contracts the product and updates the artifacts

3. Modified, but not yet saved, orders are passed to `ItemWriter` to save them.

### Concurrency and locking

To prevent concurrent execution of the process phases on one order, every step uses two techniques:

1. Checking whether the order is in the expected state; e.g. the step responsible for contracting the products checks whether it is in state `CUSTOMER_REGISTRATION_COMPLETED`
2. To prevent concurrent reads from other application instances, before running the processing the order is transitioned to one of the `*IN_PROGRESS` states with `select-for-update`  lock.

### Idempotency & retries

Spring batch gives a possibility to make steps executions fault tolerant. Usual configuration of the order processing step will retry the processing of the particular order on every exception 3 times with 3 seconds of backoff. After 3 failures the order gets skipped.

## Endpoints

| Endpoint                                                     | Description                      |
| ------------------------------------------------------------ | -------------------------------- |
| `POST http://localhost:8180/orders`                          | Issue an order                   |
| `GET http://localhost:8180/orders/{orderId}`                 | Reading an order                 |
| `POST http://localhost:8181/customer-registrations`          | Creating a customer registration |
| `GET http://localhost:8181/customer-registrations/{registrationId}` | Reading a customer registration  |
| `POST http://localhost:8182/product-activations`             | Activating of the product        |
| `GET http://localhost:8182/product-activations/{activationId}` | Reading a product activation     |

